[toc]

## 安装

安装和调试是笔记中最重要的部分了属于是。

vscode内置emmet，但是vscode是用electron做的，启动速度比notepad++慢了不是一点点（虽然已经很快了，但我每次想到electron“又开了一个chrome”来吃我内存就不想用它）。vscode野蛮，npp文明。

但文明的npp不仅不能直接在插件管理器里安装emmet，连emmet官方的插件都十年没有更新！而且还是用臃肿python写的script，还要先安装一个比npp大几倍的python script插件！这是我不能容忍的。

好在我发现了npp有js脚本管理器，还有人和我有一样的想法做了[js版的emmet](https://github.com/sieukrem/jn-npp-plugin) 。

> Apparently, current Emmet plugin for Notepad++ is extreme slow on big file ([emmetio/npp#2](https://github.com/emmetio/npp/issues/2)). This version is much faster although I don't know where is the problem :|
> 现在我已经发现的问题有
> 1 .class和#id输出异常
> 2 ctrl+d无法匹配标签
> 3 ctrl+p是注释而不是官方语法中的ctrl+/
> 4 

怎么安装？自己看readme去

哦，对了千万别忘了先把编辑器的语言设置成HTML而不是纯文本，否则就会犯像我一样像到处找在自己手里的手机一样的错误。


## 基本语法

[参考文章]:https://code.z01.com/Emmet/	"Emmet快速语法"

### 后代>
div>p
```
<div><p></p></div>
```


### 兄弟+
div+p
```
<div></div>
<p></p>
```

### 上级^
div>span^p
```
<div><span></span></div>
<p></p>
```

### 分组()

常和加号配合使用

```div>(header>ul>li*2>a)+footer>p```

```html
<div>
    <header>
        <ul>
            <li><a href=""></a><li>
            <li><a href=""></a><li>
        </ul>
    </header>
    </footer>
        <p></p>
    </footer>
</div>
```

### 乘法：*



### 自增符号：$

顾名思义，自增即自动增加的东西。

#### 正自增

* ul>li.item$*5
```
<ul>
    <li class="item1"></li>
    <li class="item2"></li>
    <li class="item3"></li>
    <li class="item4"></li>
    <li class="item5"></li>
<ul>
```

* h$[title=item$]{Header $}*3
```
<h1 title="item1">Header 1</h1>
<h2 title="item2">Header 2</h2>
<h3 title="item3">Header 3</h3>
```

* `ul>li.item$$$*5`

```
<ul>
    <li class="item001"></li>
    <li class="item002"></li>
    <li class="item003"></li>
    <li class="item004"></li>
    <li class="item005"></li>
</ul>
```
#### 倒自增@-

ul>li.item$@-*5
```
<ul>
    <li class="item5"></li>
    <li class="item4"></li>
    <li class="item3"></li>
    <li class="item2"></li>
    <li class="item1"></li>
</ul>
```

#### 从某个数开始自增@x
ul>li.item$@3*5
```
<ul>
    <li class="item3"></li>
    <li class="item4"></li>
    <li class="item5"></li>
    <li class="item6"></li>
    <li class="item7"></li>
</ul>
```
### #ID和.类属性

直接输入#id会输出（这好像叫做隐式标签）
`<div id="id"></div>`
同理，.class会输出
`<div class="class"></div>`

也可以组合输入.1#1，输出
`<div class="1" id="1"></div>`

或一次输入多个类和ID，.1.2.3 输出
`<div class="1 2 3"></div>`

### 自定义属性[]

[a=value1 b=value2]
```
<div a="value1" b="value2"></div>

```

td[rowspan=2 colspan=3 title]
```
<td rowspan="2" colspan="3" title=""></td>
```

### 标签文本{}

a{Click me}
```
<a href="">Click me</a>
```

p>{Click }+a{here}+{ to continue}
```
<p>Click <a href="">here</a> to continue</p>
```

### 隐式标签
在许多情况下可以省略标签名，Emmet 会妥善处理。比如不写 div.content 而写 .content，可以展开为 `<div class="content"></div>` 
How it works 工作原理

当展开缩写时，Emmet 尝试获取缩写所处位置的父元素上下文，比如 HTML 元素。如果获取成功，Emmet 使用它的名字来解析隐式标签名：

下面缩写隐式与显式标签名输出一致：

| 隐式  | 显式 |
---|---
| `.wrap>.content`  | `div.wrap>div.content` |
| `em>.info`              | `em>span.info`|
| `ul>.item*3`           | `ul>li.item*3`           |
| `table>#row$*4>[colspan=2]` | `table>tr#row$*4>td[colspan=2` |

### “Lorem Ipsum” 生成器

输入lorem或lipsum（不明白为什么前者和上面一样，后者不一样）生成假文用于填充文章测试用。
默认生成30字的文章，可以用lorem100生成100字的，以此类推。

## HTML标签语法

### 所有未知的缩写都会转化成标签

### 基本HTML标签
！输出HTML基本结构

#### a
a 输出
`<a href=""></a>`

a:link输出
`<a href="http://"></a>`

a:mail
展开输出↓

<a href="mailto:"></a>


#### base

base 设置根href
`<base href="">`
basefont 用来设置文档的默认字体大小
<basefont />

#### bdo
bdo:r  文字从右往左
bdo:l  文字从左往右

#### link
link:css
`<link rel="stylesheet" href="style.css" />`

link:print 不知道什么用
`<link rel="stylesheet" href="print.css" media="print" />`

link:favicon 设置tab栏小图标
`<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />`

link:touch 苹果专用属性，设置safari将网站添加到桌面后的图标
`<link rel="apple-touch-icon" href="favicon.png" />`


ink:rss 对rss还不太了解
```
<link rel="alternate" type="application/rss+xml" title="RSS"`href="rss.xml" />
```

link:atom  也不懂
```
<link rel="alternate" type="application/atom+xml" title="Atom" href="atom.xml" />
```

#### meta
我还不太熟悉meta的属性的含义，明天再写

meta:utf 
`<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />`

meta:compat 兼容性（因为垃圾的IE）
`<meta http-equiv="X-UA-Compatible" content="IE=7" />`

顺便，讲讲http-equiv是什么。
为什么要用equiv这个几乎查不到意思的词，我也不得而知。这个词的大概意思是“简单的，易懂的”。
http-equive定义要设置的东西是什么，content写具体的设定值。

> 属性定义了一个编译指示指令。这个属性叫做 http-equiv(alent) 是因为所有允许的值都是特定HTTP头部的名称（MDN的官方解释，没看懂），如下：
content-security-policy
它允许页面作者定义当前页的内容策略。 内容策略主要指定允许的服务器源和脚本端点，这有助于防止跨站点脚本攻击。
content-type
如果使用这个属性，其值必须是"text/html; charset=utf-8"。注意：该属性只能用于 MIME type 为 text/html 的文档，不能用于MIME类型为XML的文档。
default-style
设置默认 CSS 样式表组的名称。
>
> x-ua-compatible
如果指定，则 content 属性必须具有值 "IE=edge"。用户代理必须忽略此指示。
>
> refresh
这个属性指定:
如果 content 只包含一个正整数，则为重新载入页面的时间间隔(秒)；
如果 content 包含一个正整数，并且后面跟着字符串 ';url=' 和一个合法的 URL，则是重定向到指定链接的时间间隔(秒)


meta:vp 视窗大小
```
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
```


#### script
script:src
`<script src=""></script>`

## CSS

没多少东西，编辑器自带的补全差不多

而且有模糊搜索，即使记不下来那么多css缩写也不用担心。

如：缩写不是 `ov:h`(`overflow: hidden;`) ，而是 `ov-h`, `ovh` 甚至 `oh`，都可以达到同样的效果。

## 快捷键

#### 展开缩写

Tab，不用多说了

#### 匹配标签

`ctrl+d`向外匹配，`ctrl+shift+d`向内匹配
从插入符所在位置开始查找标签及标签内容，并选中它。当多次调用时将向外扩展或向内收缩选择。由于实现上的问题，不是所有的编辑器同时支持这两种操作，多数编辑器只支持向外扩展选择。

#### 跳转到配对标签
`ctrl+t` 在HTML标签的开始与关闭标记间跳转。

#### [包裹缩写](https://yanxyz.github.io/emmet-docs/actions/wrap-with-abbreviation/)（重要）
**这是 Emmet 的一个强大工具**，展开缩写，**并将选中的内容放在最后生成的元素内**。如果没有选择，则调用匹配标签功能，包裹匹配的元素。

##### 包裹单行

选中要包裹的内容，`ctrl+shift+a` 后在弹出框中正常输入缩写即可（vscode需要先用ctrl+shift+p搜索wrap找到这个命令后绑定快捷键）

##### 包裹多行

选中要包裹的多行内容，`ctrl+shift+a` 后

#####  Removing list markers 删除列表标记

从其它地方复制的表格在粘贴时会带着开头的列表项，而且是多行的，在缩写的最后加上`|t`即可删除。

#####  Controlling output position 控制插入位置

没懂

#### 跳转到编辑点
这个功能适用于 HTML 代码块，可以在要点之间跳转：

between tags 标签之间
empty attributes 空标签
newlines with indentation 缩进的新行

* “Next Edit Point” (`Ctrl+Alt+→`)
* “Previous Edit Point” (`Ctrl+Alt+←`)

**但我实测vscode和notepadd都用不了。**

#### 选择

这个功能类似于 编辑点间移动 功能，但是选择重要的代码部分。

- “Select Next Item” (`Shift+Ctrl+.`)
- “Select Previous Item” (`Shift+Ctrl+,`)

#### 切换注释

`ctrl+/` 

npp的插件是用`ctrl+q`

#### 分割合并标签
`ctrl+j`

这个功能分割合并标签，例如将`<tag/>` 转换为` <tag></tag>` ，或逆操作。对于 XML/XSL 开发很有用。（但我不会xml）

#### 删除标签
`ctrl+k`快速删除标签，并调整缩进。这里的标签是在插入符所在位置由 匹配标签 功能查找的标签。

##### 合并行

`ctrl+shift+m`许多编辑器有类似功能：将选中的多行合并为单行。不过如果没有选择，Emmet 将匹配所在 HTML 标签。

##### 更新图片尺寸

`ctrl+shift+u`也有可能是`ctrl+u`

许多开发者忘记给 `<img>` 标签添加 *width* 与 *height* 属性，这样对用户体验不好。这个功能自动帮你搞定：将插入符放在 `<img>` 标签内，然后调用此功能，就可以添加或更新 width 与 height 属性。

**对于 CSS**，将插入符放在属性值 `url()` 内，然后调用此功能，就可以添加或更新 width 与 height 属性。

##### 计算数学表达式
`Shift+Ctrl+Y`
计算简单的数学表达式，比如2*4 或 10/2，并输出结果。\\ 操作符结果同 round(a/b)。


##### 增减数字
以不同的步值增减插入符所在位置的数字。
“Increment by 1” (`Ctrl+↑`)
“Decrement by 1” (`Ctrl+↓`)
“Increment by 0.1” (`Alt+↑`)
“Decrement by 0.1” (`Alt+↓`)
“Increment by 10” (`Ctrl+Alt+↑`)
“Decrement by 10” (`Ctrl+Alt+↓`)

##### CSS重构
`ctrl+b`
对 CSS 开发很实用的功能： 将插入符所在的 CSS 属性值复制给带厂商（即mozilla，apple，chrome，IE）前缀的属性中。

##### 编码/解码图像为 data:URL

`shift+ctrl+i`

HTML 与 CSS 可以用 [data:URL](http://en.wikipedia.org/wiki/Data_URI_scheme) 插入外部资源。通常用在线服务或第三方软件将图像转换为 base64。

但是这些工具有缺点：在线工具要花费额外的时间，或者不能控制图片是否应该转换为 base64。

使用 Emmet 在编辑器里就可以将图像转换为 base64，或者相反操作（相反好像不行）。
