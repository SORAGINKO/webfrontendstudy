# JavaScript语法部分

## 1 简介

以后复习时再写

## 2 JS基础知识

### 2.3 现代模式，“use strict"

### 2.4变量

### 2.5数据类型

### 2.6三种交互方式 alert() prompt() confirm()

### 2.7 类型转换

### 2.8 基础运算符

### 2.9 值的比较

### 2.10条件分支：if和？

### 2.11四个逻辑运算符“||，&&，！，？？“

### 2.12专讲空值合并运算符？？

### 2.13循环：while和for

### 2.14 switch语句（好像不怎么用）

### 2.15 函数

### 2.16 函数表达式

无论函数是如何创建的，函数都是一个值。

```js
function sayHi() {alert( "Hello" );} //函数声明，多用，创建前就可使用
let sayHi() = function() {alert("Hello";} //函数表达式，少用，未创建前不可使用
alert( sayHi ); // 显示函数代码而不是调用函数
sayHi()才能调用函数
在某些编程语言中，只要提到函数的名称都会导致函数的调用执行，但 JavaScript 可不是这样。

let func = sayHi;    // 复制函数本身到func
let func = sayHi();	 //将函数的结果赋值给func
```

### 2.17 箭头函数，基础知识

`let func = (arg1, arg2, ..., argN) => expression;`

这里创建了一个函数 func，它接受参数 arg1..argN，然后使用参数对右侧的 expression 求值并**返回**其结果。（返回！！！）

```js
let result = (item) => item;
相当于
let result = function(item) {return item;}
```

### 2.18 JS特性（本章总结）

## 3 代码质量（没多少东西，不写了）

## 4 Object(对象)：基础知识（重要）

### 4.1 对象基础

用点符号访问属性值不用加字符串，用[]访问属性值里一定要加字符串，
如现在有一个对象users = {name: 'name'}，那么`users.name或users['name']`都可以访问到name，但users[name]不可以，除非name是个值为'name'的变量。

### 4.2 对象引用和复制

### 4.3 对象垃圾回收

### 4.4 对象方法!，this

**作为对象属性的函数被称为 方法。**
通常，对象方法需要访问对象中存储的信息才能完成其工作。

在 JavaScript 中，`this` 关键字与其他大多数编程语言中的不同。JavaScript 中的 `this` 可以用于任何函数，即使它不是对象的方法。

### 4.5 构造器和操作符“new“（这章卡了我半年没学）

构造器可以理解为常归函数，但是函数首字母必须大写，而且里面有很多`this`。

构造函数只能通过new操作符来调用，这样的调用意味着在开始时创建了空的 `this`，并在最后返回填充了值的 `this`。



### 4.6 可选链“?."

### 4.7 数据类型之symbol类型

### 4.8 对象的原始值转换

1. **对象到原始值的转换，是由许多期望以原始值作为值的内建函数和运算符*自动调用*的。**

   这里有三种类型（hint）：

   - `"string"`（对于 `alert` 和其他需要字符串的操作）
   - `"number"`（对于数学运算）
   - `"default"`（少数运算符，通常对象**以和 `"number"` 相同的方式实现 `"default"` 转换**）

   **规范明确描述了哪个运算符使用哪个 hint。**

   转换算法是：

   1. 调用 `obj[Symbol.toPrimitive](hint)` 如果这个方法存在，
   2. 否则，如果 hint 是 "string"，尝试调用 `obj.toString()` 或 `obj.valueOf()`，无论哪个存在。
   3. 否则，如果 hint 是 ，"number"或"default"尝试调用 `obj.valueOf()` 或 `obj.toString()`，无论哪个存在。

   **所有这些方法都必须返回一个原始值才能工作**（如果已定义？）。

   在实际使用中，**通常只实现 `obj.toString()` 作为字符串转换的“全能”方法就足够了**，该方法应该返回对象的“人类可读”表示，用于日志记录或调试。

## 5 数据类型专讲（重要）

### 5.1 原始类型及其方法

JS中几乎一切都是对象，原始类型可以看作不能添加自定义方法的对象，我们可以像对象一样使用原始类型。

在 JavaScript 中有且只有 7 种原始类型：`string`，`number`，`bigint`，`boolean`，`symbol`，`null` 和 `undefined`。

特别注意：null和undefined没有任何方法

### 5.2 数字类型（重要）

1. 1000000 = 1e16
2. 0.0000001 = 1e-6
3. `alert(0xff或0o377或0b11111111)`，会自动转换成十进制255
4. num.toString(base)
   这种`某个对象或原始类型.method()`属于**方法**，而不带()的`.property`叫属性，num的那几个属性不太常使用。
   其实num也是标准内置对象（本质是window这个对象里的属性）的一种，因为可以用`window.number()`来创建number
5. 舍入`Math.floor/ceil/round/trunc`的区别
   这种属于**标准内置对象的数字计算对象**，
6. JS用64位存储数字带来的精度问题
7. I`sNaN(value)`测试value是否为NaN，`IsFinite(value)`测试是否为非NaN/InFinity/-InFinity，都返回布尔值。
   NaN/InFinity/-InFinity都属于数字类型的一种，NaN不等于除了它自身的任何值
   这两种属于**标准内置对象的全局函数**，
8. `parseInt(str[,base(2-36可选)]) parseFloat(str[,base(2-36可选)])`解析如100px的字符串中的数字。
   这两个也是**标准内置对象的全局函数**

### 5.3 字符串（重要）

1. 三种引号："str", 'str', \`str\`，前两个用法在JS里基本相同，反引号允许我们通过 `${…}` 将任何表达式嵌入到字符串中：

```js
function sum(a, b) {
  return a + b;
}

alert(`1 + 2 = ${sum(1, 2)}.`); // 1 + 2 = 3.
```

反引号的另一个优点是它们允许字符串跨行。

2. 特殊字符，也就是转义字符，用backslash\开头

| 字符                                    | 描述                                                         |
| --------------------------------------- | ------------------------------------------------------------ |
| `\n`                                    | 换行                                                         |
| `\r`                                    | 在 Windows 文本文件中，两个字符 `\r\n` 的组合代表一个换行。而在非 Windows 操作系统上，它就是 `\n`。这是历史原因造成的，大多数的 Windows 软件也理解 `\n`。 |
| `\'`, `\"`                              | 引号                                                         |
| `\\`                                    | 反斜线                                                       |
| `\t`                                    | 制表符                                                       |
| `\b`, `\f`, `\v`                        | 退格，换页，垂直标签 —— 为了兼容性，现在已经不使用了。       |
| `\xXX`                                  | 具有给定十六进制 Unicode `XX` 的 Unicode 字符，例如：`'\x7A'` 和 `'z'` 相同。 |
| **`\uXXXX`**                            | 以 UTF-16 编码的十六进制代码 `XXXX` 的 Unicode 字符，例如 `\u00A9` —— 是版权符号 `©` 的 Unicode。它必须正好是 4 个十六进制数字。 |
| `\u{X…XXXXXX}`（1 到 6 个十六进制字符） | 具有给定 UTF-32 编码的 Unicode 符号。一些罕见的字符用两个 Unicode 符号编码，占用 4 个字节。这样我们就可以插入长代码了。 |

3. 字符串**属性**length
4. 像数组一样访问字符[]
5. 字符串是不可改变其中一个字符的，只能用新的重新替代
6. toLowerCase() 和 toUpperCase() **方法**可以改变大小写：
7. 查找字符串的几种方法

查找字符串的方法 | 使用方法和返回值
------------------------- | -----------------------
str.indexOf(substr, pos) | 从给定位置 `pos` 开始，在 `str` 中查找 `substr`，如果没有找到，则返回 `-1`，否则返回substr的索引 
sstr.lastIndexOf(substr, pos) | 反过来
if (~str.indexOf(...)) 读作 “if found” | 用 [bitwise NOT](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_NOT) `~` 运算符来当作if判定条件的老技巧，不如用更现代的include方法 
str.includes(substr, pos) | 根据 str 中是否包含 substr 来返回 true/false，代替上边这个老技巧 
str.startsWith 和 str.endsWith | 判断当前字符串是否以另外一个给定的子字符串开头或结束 

8. 获取字符串的三种方法

| 方法                    | 选择方式……                                | 负值参数            |
| ----------------------- | ----------------------------------------- | ------------------- |
| `slice(start, end)`     | 从 `start` 到 `end`（不含 `end`）         | 允许                |
| `substring(start, end)` | 从 `start` 到 `end`（不含 `end`）         | 负值被视为 `0`      |
| `substr(start, length)` | 从 `start` 开始获取长为 `length` 的字符串 | 允许 `start` 为负数 |

9. 正确的比较字符串的方法

不能正确比较的原因：

字符串按字母顺序逐字比较。但小写字母总是大于大写字母：`alert( 'a' > 'Z' ); // true`，而且带变音符号的字母存在“乱序”的情况：`alert( 'Österreich' > 'Zealand' ); // true`因为所有的字符串都使用 [UTF-16](https://en.wikipedia.org/wiki/UTF-16) 编码。即：每个字符都有对应的数字代码。**有特殊的方法可以获取代码表示的字符，以及字符对应的代码。**

`str.codePointAt(pos)`返回在 `pos` 位置的字符代码 :                                                                                                                 ``

```javascript
// 不同的字母有不同的代码
alert( "z".codePointAt(0) ); // 122
alert( "Z".codePointAt(0) ); // 90
String.fromCodePoint(code)
```

通过数字 `code` 创建字符`alert( String.fromCodePoint(90) ); // Z`

正确的比较方法：

调用 [str.localeCompare(str2)](https://developer.mozilla.org/zh/docs/Web/JavaScript/Reference/Global_Objects/String/localeCompare) 会根据语言规则返回一个整数（默认语言从环境中获取，字符顺序视语言不同而不同）

- 如果 `str` 排在 `str2` 前面，则返回负数。
- 如果 `str` 排在 `str2` 后面，则返回正数。
- 如果它们在相同位置，则返回 `0`。

### 5.4 数组（重要）

1. 用arr = [];而不是arr = new Array();这种麻烦的方法来**声明数组**。
2. arr[index]不支持负数，用arr.at(index)可以用**负数索引**。
3. 四个常用的**数组方法**，操作末端的push,pop性能快且常用，操作首端的shift,unshift速度慢不常用。
4. 专为数组设计的for...of循环

`for..of` 不能获取当前元素的索引，只是获取元素值，但大多数情况是够用的。而且这样写更短。

for..in` 循环会遍历 **所有属性**，不仅仅是这些数字属性。`for..in` 循环适用于普通对象，并且做了对应的优化。但是不适用于数组，因此速度要慢 10-100 倍。

通常来说，我们不应该用 `for..in` 来处理数组。

5. length属性是可写的，结果会反应到数组本身
   清空数组最简单的方法就是：`arr.length = 0;`。
6. toString方法在alert里会自动使用
   数组没有 `Symbol.toPrimitive`，也没有 `valueOf`，它们只能执行 `toString` 进行转换。
7. 不要使用 == 比较数组
   JavaScript 中的数组与其它一些编程语言的不同，不应该使用 == 运算符比较 JavaScript 中的数组。因为仅当两个对象引用的是同一个对象时，==才返回true

### 5.5 数组方法（重要，内容多，13个习题做了两天）

1. 添加/移除元素的现代**方法**

   **arr.splice()**

   英语意思是拼接，可以删除插入，并返回删除的的元素
   语法：`arr.splice(start[, deleteCount, elem1, ..., elemN])`返回被删除的元素所组成的数组。
   举例：

   * arr.splice(1, 1，'str1', 'str2'); // 从索引 1 开始删除 1 个元素，并用两个字符串代替
   * arr.splice(2, 0, 'str1', 'str2'); // 从索引2开始插入两个字符串
   * arr.splice(-1, 0, 'str1', 'str2'); //从最后第二个元素插入两个字符串

   **arr.slice**
   语法：`arr.slice([start], [end]);`返回所选择的新数组，
   不带参数的调用它会创建一个arr的副本。
   和字符串的 `str.slice` 方法有点像，就是把子字符串替换成子数组。

   arr.concat()不常用，习题中都没有用到
   语法：`arr.concat(arg1, arg2...)`
   它接受任意数量的参数 —— 数组或值都可以。
   结果是一个包含来自于 `arr`，然后是 `arg1`，`arg2` 的元素的新数组。

2. 遍历：arr.forEach(fn)和**arr.map(fn)**的区别
    它们都能为数组中的每个元素运行一个函数，但forEach不返回任何结果，所以不如map常用。

3. 搜索数组

  用于纯数组的方法：

  indexOf()/lastIndexOf()返回索引而不是元素，不常用。

  includes()返回布尔值，常用于if条件判定，并且includes可以正确判定NaN是否存在。

  用于对象数组的方法：

**find()**，**filter()**，findIndex()，findLastIndex()，后两个不太常用 

  ```js
  find(function(item[, index, array]) { 
      //一般我们只用item，index偶乐用，array几乎不用
      //并且这里一般写成箭头函数
      //判定条件，若true，则返回这个item并停止
      //若没有搜索到，则返回undefined
  }
  ```
例子：

```js
let users = [
  {id: 1, name: "John"},
  {id: 2, name: "Pete"},
  {id: 3, name: "Mary"}
];

let user = users.find(item => item.id == 1);

alert(user.name); // John
```

filter(fn)和find(fn)语法一样，但filter()返回所有匹配值组成的数组，判定条件也常是比大小。

4. **转换数组（内容多）**

   **map(fn)**因为可以返回每个元素处理后的值，因此也可以用来生成新数组。

   **sort{[fn])**进行原位排序，即改变原数组，但还返回值，不过没什么用。
   若参数为空，则进行UTF-16字符代码排序，没什么用。
   要使用我们自己的排序顺序，我们需要提供一个函数作为 `arr.sort()` 的参数。该函数应该比较两个任意值并返回：

   ```javascript
   function compare(a, b) {
     if (a > b) return 1; // 如果第一个值比第二个值大
     if (a == b) return 0; // 如果两个值相等
     if (a < b) return -1; // 如果第一个值比第二个值小
   }
   不过不用写的这么麻烦，可以这样写
   arr.sort(function(a, b) { return a - b; });
   也可以这样写
   countries.sort( (a, b) => a > b ? 1 : -1) )
   最简便的方法是这样
   arr.sort( (a, b) => a - b );
   但比较字符串还是要用localeCompare
   countries.sort( (a, b) => a.localeCompare(b) )
   ```

   arr.reverse(没参数)进行原位颠倒，并返回颠倒后的数组，和sort一样返回值没什么用了。

   **str.split(delim)** 方法给定的分隔符 delim 将字符串分割成一个数组。**arr.join(glue)** 会在它们之间创建一串由 glue 粘合的 arr 项。

   map()升级版——reduce()/reduceRight()
   语法是：

   ```javascript
   let value = arr.reduce(function(accumulator, item, index, array) {
     // ...
   }, [initial]);
   ```

   掌握这个知识点的最简单的方法就是通过示例。

   在这里，我们通过一行代码得到一个数组的总和：

   ```javascript
   let arr = [1, 2, 3, 4, 5];
   
   let result = arr.reduce((sum, current) => sum + current, 0);
   
   alert(result); // 15
   ```

5. Array.isArray(arr)判断是否数组

### 5.6 可迭代对象

