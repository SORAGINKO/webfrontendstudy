[TOC]

[来源:阿里云开发者课堂前端开发学习路线&FCC](https://edu.aliyun.com/roadmap/frontend)

## JavaScript是什么，学什么

&emsp;&emsp;在2015年以前，JS发展缓慢，而且经常改名。ES4由于改进较大还被抛弃了（后来又在ES6里重现了）。201年以后，ECMA决定每年起一个版本号，所以ES6也叫做ECMAScript2015，依此类推，ECMAScript2016、2017……2022。

&emsp;&emsp;JS是需要依赖宿主的语言就不多说了，我也知道。学习JS主要学习两个方面：语言结构&宿主环境提供的API。前者是和普通的编程语言类似，学习一些语法规则 变量 流程控制 函数还有内置标准库对象; 后者需要学习BOM，DOM，网络控制类如HTTP WebSocket。

&emsp;&emsp;

## JavaScript语言结构

## 如何在HTML中插入JS

* 内嵌法（不推荐）

即在HTML文件内插入`<script>`标签，可以在head也可以在body末尾。

* head引入（不推荐）

原来没有这种方式，我以为和CSS一样用`<link href="" />`呢

* body末尾引入

用形如`<script src="js/1.js">这里不用写代码了，写了也不执行</script>`的方式

## 如何写注释
`//这是一个单行js注释`

`/* 这是一个多行js注释
这是一个多行js注释 */`

## 变量

### 数据类型
 JavaScript 提供七种不同的数据类型，它们是
 `undefined`（未定义）、
 `null`（空）、
 `boolean`（布尔型）、
 `string`（字符串）、用单引号或双引号包裹起来的
 `symbol`、
 `number`（数字）、
 `bigint`（可以表示任意大的整数）
 `object`（对象）。

数字，例如 `12`，和由字符组成的字符串 `strings`，例如 `"12"`、`"dog"` 或 `"123 cats"`。 
计算机可以对数字执行数学运算，但不能对字符串执行数学运算。

变量非常类似于你在数学中使用的 x、y 变量，都是以一个简单命名的名称来代替我们赋值给它的数据。 计算机中的变量与数学中的变量不同的是，计算机可以在不同的时间存储不同类型的变量。

### 声明变量并赋值

ES6之前只能用var来声明变量和后续的赋值

* 赋值单个变量

  `var 变量名 = 值;

* 赋值多个变量

  ` var var1 = value, var2 = value;

&emsp;&emsp;**变量名只能由数字，字母，下划线和$组成**，并且**不能以数字开头**；**变量的名字也不能是诸如for while if等**控制语句。另外，变量名是区分大小写的。惯例是使用驼峰命名法（camelCase），如userName

&emsp;&emsp;当 JavaScript 中的变量被声明（还没被赋值）的时候，程序内部会给它一个初始值 undefined。 当你对一个值为 undefined 的变量进行运算操作的时候，算出来的结果将会是 NaN，它的意思是 "Not a Number"。 当你用一个值是 undefined 的变量来做字符串拼接操作的时候，它会转换成字符串（string）undefined。

* 用let声明变量重新赋值会报错，替代var（因为var可以被重新声名，这会带来问题），用驼峰命名法。
* 用 const声名的变量不仅不能重新赋值，而且必须在声名的时候赋值，用全部大写命名法。

>  **注意：** 对于不可变值，开发人员通常使用大写变量标识符，对可变值（对象和数组）使用小写或驼峰式标识符。

[JavaScript中var, let, const的区别]:https://chinese.freecodecamp.org/news/javascript-var-let-and-const

## 转义字符

和css中一样，用正斜杠`\`

此外还有一此特殊的如 

| 代码 |  输出  |
| :--: | :----: |
| `\'` | 单引号 |
| `\"` | 双引号 |
| `\\` | 反斜杠 |
| `\n` | 换行符 |
| `\r` | 回车符 |
| `\t` | 制表符 |
| `\b` |  退格  |
| `\f` | 换页符 |



## 四则运算和递增/减
### 四则运算
加减乘除 `+-*/*`

整数和小数都一样用，但JS的浮点运算有精度问题，比如0.1+0.2 =0.30000000000000004。

### 自增、自减
使用 ++，我们可以很容易地对变量进行自增或者 +1 运算。
`i++;`等效于：`i = i + 1;`

使用自减符号 --，你可以很方便地对一个变量执行自减或者 -1 运算。
`i--;`等效于：`i = i - 1;`

### 其它

* remainder 求余运算符 % 返回两个数相除得到的余数
  用法
  在数学中，判断一个数是奇数还是偶数，只需要判断这个数除以 2 得到的余数是 0 还是 1。

  17 % 2 = 1（17 是奇数）
  48 % 2 = 0（48 是偶数）
  提示余数运算符（remainder）有时被错误地称为“模数”运算符。 它与模数非常相似，但不能用于负数的运算。

* 复合赋值之 +=
  在编程中，通常通过赋值来修改变量的内容。 **记住，赋值时 JavaScript 会先计算等号右边的内容**
  `myVar = myVar + 5;`
  还有一类操作符是一步到位，既做运算也赋值的。其中一种就是 += 运算符。
   ` myVar += 5;`
  
* 复合赋值之 -=
  与 += 操作符类似，-= 操作符用来对一个变量进行减法赋值操作。

  `myVar = myVar - 5;`等价于：`myVar -= 5;`
  
* 复合赋值之 *=
*= 操作符是让变量与一个数相乘并赋值。
`myVar = myVar * 5;` 等价于：`myVar *= 5;`

* 复合赋值之 /=
  /= 操作符是让变量与另一个数相除并赋值。
  `myVar = myVar / 5;` 等价于：`myVar /= 5;`

***

一星期没学

两星期没学

***

## 字符串

### 连接字符串

* 用+连接

```js
const ourStr = "I come first. " + "I come second.";
```
或
```js
const ourName = "freeCodeCamp";
const ourStr = "Hello, our name is " + ourName + ", how are you?";
```

* 用+=连接

```js
let ourStr = "I come first. ";
ourStr += "I come second.";
```
或

```js
const anAdjective = "awesome!";
let ourStr = "freeCodeCamp is ";
ourStr += anAdjective;
```

### 查找字符串的长度

你可以通过在字符串变量或字符串后面写上 `.length` 来获得 `String` 的长度。

```js
console.log("Alan Peter".length);
```

### 字符串索引

方括号表示法（Bracket notation）是一种在字符串中的特定 index（索引）处获取字符的方法。

大多数现代编程语言，如 JavaScript，不同于人类从 1 开始计数。 它们是从 0 开始计数。 这被称为基于零（Zero-based）的索引。

例如，单词 `Charles` 的索引 0 的字符是 `C`。 所以如果 `const firstName = "Charles"`，你可以通过 `firstName[0]` 得到字符串第一个字母的值。同理，你可以用firstName[1]来得到第二个字母。

示例：

```js
const firstName = "Charles";
const firstLetter = firstName[0];
```

`firstLetter` 值为字符串 `C` 。



但是，如果字符串很长，你没有功夫一个一个去数，所以

获取字符串的最后一个字符，可以用字符串的长度减 1 的索引值。

例如，如果 `const firstName = "Ada"`，则可以使用 `firstName[firstName.length - 1]` 获取字符串最后一个字母的值。同理，可以使用 `firstName[firstName.length - 2]`来获得倒数第二个字符。

示例：

```js
const firstName = "Ada";
const lastLetter = firstName[firstName.length - 1];
```

`lastLetter` 值为字符串 `a`。

### 字符串的值是immutable

在 JavaScript 中，字符串（`String`）的值是不可变的（immutable），这意味着一旦字符串被创建就不能被改变。

例如，下面的代码：

```js
let myStr = "Bob";
myStr[0] = "J";
```

是不会把变量 `myStr` 的值改变成 `Job` 的，因为变量 `myStr` 是不可变的。 注意，这*并不*意味着 `myStr` 永远不能被改变，只是字符串字面量 string literal 的各个字符不能被改变。 改变 `myStr` 的唯一方法是重新给它赋一个值，例如：

```js
let myStr = "Bob";
myStr = "Job";
```




## 数组

使用数组（`array`），我们可以在一个地方存储多个数据。

以左方括号开始定义一个数组，以右方括号结束，里面每个元素之间用逗号隔开，例如：

```js
const sandwich = ["peanut butter", "jelly", "bread"];
```

您也可以在其他数组中嵌套数组，如：

```js
const teams = [["Bulls", 23], ["White Sox", 45]];
```

这也叫做多维数组（multi-dimensional array）。

### 数组索引查找

我们可以使用索引（indexes）来访问数组中的数据。

* 一维数组

数组索引与字符串一样使用方括号来表示，不同的是，它们不是指定字符，而是指定数组中的一个条目。 数组索引与字符串索引一样是从 0 开始（zero-based）的，所以数组中第一个元素的索引编号是 `0`。

```js
const array = [50, 60, 70];
array[0];
const data = array[1];
```

现在 `array[0]` 的值是 `50`， `data` 的值为 `60`.

**注意：****数组名与方括号之间不应该有任何空格**，比如`array [0]` 。 尽管 JavaScript 能够正确处理这种情况，但是当其他程序员阅读你写的代码时，这可能让他们感到困惑。

* 多维数组

我们可以把多维数组看作成是*数组中的数组*。 使用方括号表示法访问数组时，第一个方括号访问的是数组的最外层（第一层），第二个方括号访问的是数组的第二层，以此类推。

**例如：**

```js
const arr = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
  [[10, 11, 12], 13, 14]
];

arr[3];
arr[3][0];
arr[3][0][1];
```

`arr[3]` 为 `[[10, 11, 12], 13, 14]`，`arr[3][0]` 为 `[10, 11, 12]`，并且 `arr[3][0][1]` 为 `11`。

**注意：** **同样，数组名与方括号之间不应该有任何空格**，但是数组和数组之间也是有`，`的请多加注意。

### 数组索引修改

与字符串不同，数组的条目是 可变的 并且可以自由更改，**即使数组是用 `const` 声明的。**

* 一维数组

**示例**

```js
const ourArray = [50, 40, 30];
ourArray[0] = 15;
```

`ourArray` 值现在为 `[15, 40, 30]`。

### 使用 push() 操作数组

一个将数据**添加到数组末尾**的简单方法是 `push()` 函数。

`.push()` 接受一个或多个参数（parameters），并把它压入到数组的末尾。

示例：

```js
const arr1 = [1, 2, 3];
arr1.push(4);

const arr2 = ["Stimpson", "J", "cat"];
arr2.push(["happy", "joy"]);
```

`arr1` 现在值为 `[1, 2, 3, 4]`，`arr2` 值为 `["Stimpson", "J", "cat", ["happy", "joy"]]`。

### 使用 pop() 操作数组

改变数组中数据的另一种方法是用 `.pop()` 函数。

`.pop()` 函数用来弹出一个数组末尾的值。 我们可以把这个弹出的值赋给一个变量存储起来。 换句话说就是 **`.pop()` 函数移除数组末尾的元素并返回这个元素。**

数组中任何类型的元素（数值，字符串，甚至是数组）都可以被弹出来 。

```js
const threeArr = [1, 4, 6];
const oneDown = threeArr.pop();
console.log(oneDown);
console.log(threeArr);
```

第一个 `console.log` 将显示值 `6`，第二个将显示值 `[1, 4]`从 `myArray` 

Question：如果我想弹出数组中的数组中的某个数呢？

### 使用 shift() 操作数组

`pop()` 函数用来移出数组中最后一个元素。 如果想要移出第一个元素要怎么办呢？

这时候我们就需要 `.shift()` 了。 它的工作原理就像 `.pop()`，但**它移除的是第一个元素，而不是最后一个**。

示例：

```js
const ourArray = ["Stimpson", "J", ["cat"]];
const removedFromOurArray = ourArray.shift();
removedFromOurArray` 值为 `Stimpson`，`ourArray` 值为 `["J", ["cat"]]
```

### 使用 unshift() 操作数组

不仅可以 `shift`（移出）数组中的第一个元素，也可以 `unshift`（移入）一个元素到数组的头部。

`.unshift()` 函数**用起来就像 `.push()` 函数一样**，但不是在数组的末尾添加元素，`unshift()` 在数组的头部添加元素。

示例：

```js
const ourArray = ["Stimpson", "J", "cat"];
ourArray.shift();
ourArray.unshift("Happy");
```

在 `shift`、`ourArray` 后值为 `["J", "cat"]`。 在 `unshift`、`ourArray` 后值为 `["Happy", "J", "cat"]`。

## 函数

### 用函数编写可重用代码

在 JavaScript 中，我们可以把代码的重复部分抽取出来，放到一个函数 （functions）中。

举个例子：

```js
function functionName() {
  console.log("Hello World");
}
```

你可以通过函数名加上后面的小括号来调用（invoke）这个函数，就像这样： `functionName();` 每次调用函数时，它都会在控制台上打印消息 `Hello World`。 每次调用函数时，大括号之间的所有代码都将被执行。

### 将值传递给带有参数的函数（A little confused)

**函数的参数 （parameters）**在函数调用中**充当传入函数的输入占位符（也叫形参）**。 函数调用时，参数可以为一个或多个。 **调用函数时输入（或传递 "passed"）的实际值被称为参数（arguments 实参）**。

这是带有两个参数的函数，`param1` 和 `param2`：

```js
function testFun(param1, param2) {
  console.log(param1, param2);
}
```

然后我们可以调用 `testFun`，就像这样： `testFun("Hello", "World");`。 我们传入了两个字符串参数， `Hello` 和 `World`。 在函数中，`param1` 等于字符串 `Hello` 以及 `param2` 等于字符串 `World`。 请注意，`testFun` 函数可以多次调用，每次调用时传递的参数会决定参数的实际值。

### 使用 return 给函数返回值

* 我们可以通过函数的参数（arguments）把值传入函数， 也可以使用 `return` 语句把数据从一个函数中传出来。

**示例**

```js
function plusThree(num) {
  return num + 3;
}

const answer = plusThree(5);
```

`answer` 的值为 `8`。

`plusThree` 带有一个参数（argument）`num`，并返回（return）一个等于 `num + 3` 的值。

* 在函数没有 `return` 语句的情况下，当你调用它时，该函数会执行内部代码，返回的值是 `undefined`。

**示例**

```js
let sum = 0;

function addSum(num) {
  sum = sum + num;
}

addSum(3);
```

`addSum` 是一个没有 `return` 语句的函数。 该函数将更改全局变量 `sum`，函数的返回值为 `undefined`。

* return 直接返回布尔值

有时人们通过 `if/else` 语句来做比较，像这样。

```js
function isEqual(a, b) {
  if (a === b) {
    return true;
  } else {
    return false;
  }
}
```

但有更好的方式来达到相同的效果。 既然 `===` 返回 `true` 或 `false` 我们可以直接返回比较结果：

```js
function isEqual(a, b) {
  return a === b;
}
```

* 函数执行到 return 语句就结束

当代码执行到 `return` 语句时，函数返回一个结果就结束运行了，return 后面的语句不会执行。

### 全局作用域和函数

在 JavaScript 中，作用域涉及到变量的作用范围。 在函数外定义的变量具有 全局 作用域。 这意味着，具有全局作用域的变量可以在代码的任何地方被调用。

**未使用 `let` 或 `const` 关键字声明的变量会在 `global` 范围内自动创建**（这并不意味着用var建立的就是全局变量）。 当在代码其他地方无意间定义了一个变量，刚好变量名与全局变量相同，这时会产生意想不到的后果。 你应该总是用 `let` 或 `const` 声明你的变量

When you add `var` before the variable name, its scope is determined by where it is placed. Like so:

```javascript
var num1 = 18; // Global scope
function fun() {
  var num2 = 20; // Local (Function) Scope
  if (true) {
    var num3 = 22; // Block Scope (within an if-statement)
  }
}
```

When you don’t, this is the result:

```javascript
num1 = 18; // Global scope
function fun() {
  num2 = 20; // Global Scope
  if (true) {
    num3 = 22; // Global Scope
  }
}
```

### 函数中的全局作用域和局部作用域

一个程序中有可能具有相同名称的局部变量 和全局变量。 在这种情况下，局部变量将会优先于全局变量。

下面为例：

```js
const someVar = "Hat";

function myFun() {
  const someVar = "Head";
  return someVar;
}
```

函数 `myFun` 将会返回字符串 `Head`，因为局部变量的优先级更高。

## 布尔值

另一种数据类型是布尔（Boolean）。 布尔值只能是两个值中的一个：`true` 或者 `false`。 它非常像电路开关，`true` 是 “开”，`false` 是 “关”。 这两种状态是互斥的。

**注意：** **布尔值是不带引号的。** 字符串 `"true"` 和 `"false"` 不是布尔值，在 JavaScript 中也没有特殊含义。

### 用 if 语句来表达条件逻辑

`if` 语句用于在代码中做出决定。 **关键字 `if` 告诉 JavaScript 在小括号中的条件为真的情况下去执行定义在大括号里面的代码。** 这种条件被称为 `Boolean` 条件，因为他们只可能是 `true`（真）或 `false`（假）。

当条件的计算结果为 `true`，程序执行大括号内的语句。 当布尔条件的计算结果为 `false`，大括号内的代码将不会执行。

**伪代码**

> if（*条件为真*）{
> *语句被执行*
> }
> 否则执行这个语句（这个语句并不是if的，而是函数本来的语句
> }

### 相等运算符==

在 JavaScript 中，有很多 相互比较的操作。 所有这些操作符都返回一个 `true` 或 `false` 值。

最基本的运算符是相等运算符：`==`。 相等运算符比较两个值，如果它们是相等，返回 `true`，如果它们不相等，返回 `false`。 **值得注意的是相等运算符不同于赋值运算符（`=`）**，赋值运算符是把等号右边的值赋给左边的变量。

```js
function equalityTest(myVal) {
  if (myVal == 10) {
    return "Equal";
  }
  return "Not Equal";
}
```

 在 JavaScript 中，**为了让两个不同的数据类型（例如 `numbers` 和 `strings`）的值可以作比较，它必须把一种类型转换为另一种类型。** 这叫作 **“类型强制转换”**。 转换之后，可以像下面这样来比较：

```js
1   ==  1
1   ==  2
1   == '1'
"3" ==  3
```

按顺序，这些表达式会返回 `true`、`false`、`true`、`true`。

### 严格相等运算符===

 与相等操作符转换数据两类型不同，**严格相等运算符不会做类型转换**。

如果比较的值类型不同，那么在严格相等运算符比较下它们是不相等的，会返回 false 。

**示例**

```js
3 ===  3
3 === '3'
```

这些条件将分别返回 `true` and `false`。

在第二个例子中，`3` 是一个 `Number` 类型，而 `'3'` 是一个 `String` 类型。

**提示** 在 JavaScript 中，你可以使用 `typeof` 运算符确定变量或值的类型，如下所示：

```js
typeof 3
typeof '3'
```

`typeof 3` 返回字符串 `number`，`typeof '3'` 返回字符串 `string`。

### 不等运算符!=

不相等运算符（`!=`）与相等运算符是相反的。 这意味着不相等并返回 `false` 的地方，用相等运算符会返回 `true`，*反之亦然*。 与相等运算符类似，不相等运算符在比较的时候也会转换值的数据类型。

**例如**

```js
1 !=  2
1 != "1"
1 != '1'
1 != true
0 != false
```

按顺序，这些表达式会返回 `true`、`false`、`false`、`false` 和 `false`。

### 严格不等运算符!==

严格不相等运算符（`!==`）与全等运算符是相反的。 这意味着严格不相等并返回 `false` 的地方，用严格相等运算符会返回 `true`，*反之亦然*。 **严格不相等运算符不会转换值的数据类型。**

**示例**

```js
3 !==  3
3 !== '3'
4 !==  3
```

按顺序，这些表达式会返回 `false`、`true`、`true`。

### 大于运算符>

使用大于运算符（`>`）来比较两个数字。 如果大于运算符左边的数字大于右边的数字，将会返回 `true`。 否则，它返回 `false`。

与相等运算符一样，大于运算符在比较的时候，会转换值的数据类型。

**例如：**

```js
5   >  3
7   > '3'
2   >  3
'1' >  9
```

按顺序，这些表达式会返回 `true`、`true`、`false` 和 `false`。

### 大于或等于运算符>=

使用大于等于运算符（`>=`）来比较两个数字的大小。 如果大于等于运算符左边的数字比右边的数字大或者相等，会返回 `true`。 否则，会返回 `false`。

与相等运算符相似，大于等于运算符在比较的时候会转换值的数据类型。

**例如：**

```js
6   >=  6
7   >= '3'
2   >=  3
'7' >=  9
```

按顺序，这些表达式会返回 `true`、`true`、`false` 和 `false`。

### 小于运算符<

使用小于运算符（`<`）来比较两个数字。 如果小于运算符左边的数字比右边的数字小，它会返回 `true`。 否则会返回 `false`。 与相等运算符类似，小于运算符在做比较的时候会转换值的数据类型。

**例如：**

```js
2   < 5
'3' < 7
5   < 5
3   < 2
'8' < 4
```

按顺序，这些表达式会返回 `true`、`true`、`false`、`false` 和 `false`。

### 小于或等于运算符<=

使用小于等于运算符（`<=`）比较两个数字的大小。 如果在小于等于运算符左边的数字小于或者等于右边的数字，它会返回 `true`。 如果在小于等于运算符左边的数字大于右边的数字，它会返回 `false`。 与相等运算符类似，小于或等于运算符会转换数据类型。

**例如**

```js
4   <= 5
'7' <= 7
5   <= 5
3   <= 2
'8' <= 4
```

按顺序，这些表达式会返回 `true`、`true`、`true`、`false` 和 `false`。

### 逻辑"与"运算符&&

有时你需要在一次判断中做多个操作。 当且仅当运算符的左边和右边都是 true，逻辑与运算符（`&&`）才会返回 `true`。

同样的效果可以通过 if 语句的嵌套来实现：

```js
if (num > 5) {
  if (num < 10) {
    return "Yes";
  }
}
return "No";
```

只有当 `num` 的值大于 `5` 并且小于`10` 时才会返回 `Yes`。 相同的逻辑可被写为：

```js
if (num > 5 && num < 10) {
  return "Yes";
}
return "No";
```

### 逻辑"或"运算符 ||

只要逻辑或运算符（`||`）两边的任何一个运算的结果是 `true`，则返回 `true`。 否则，返回 `false`。

逻辑或运算符由两个竖线（`||`）组成。 这个按键位于退格键和回车键之间。

下面这样的语句你应该很熟悉：

```js
if (num > 10) {
  return "No";
}
if (num < 5) {
  return "No";
}
return "Yes";
```

只有当 `num` 大于等于 `5` 或小于等于 `10` 时，函数才返回 `Yes`。 相同的逻辑可以简写成：

```js
if (num > 10 || num < 5) {
  return "No";
}
return "Yes";
```

## 语句

### 介绍 else 语句

当 `if` 语句的条件为真，会执行大括号里的代码。 那如果条件为假呢？ 正常情况下什么也不会发生。 使用 `else` 语句，可以执行当条件为假时相应的代码。

```js
if (num > 10) {
  return "Bigger than 10";
} else {
  return "10 or Less";
}
```

### 介绍 else if 语句

如果你有多个条件语句，你可以通过 `else if` 语句把 `if` 语句链起来。

```js
if (num > 15) {
  return "Bigger than 15";
} else if (num < 5) {
  return "Smaller than 5";
} else {
  return "Between 5 and 15";
}
```

### 使用 Switch 语句从许多选项中进行选择

如果你有非常多的选项需要选择，可以使用 switch 语句。 `switch` 评估一个表达式，将表达式的值与 case 子句匹配。 从第一个匹配的 `case` 值执行语句，直到遇到 `break`。

这是 `switch` 语句的示例：

```js
switch(lowercaseLetter) {
  case "a":（引号）
    console.log("A");（分号）
    break;（分号）
  case "b":
    console.log("B");
    break;
}
```

测试 `case` 值使用严格相等（`===`）运算符进行比较。 `break` 告诉 JavaScript 停止执行 switch 语句。 如果遗漏了 `break` ，下一个语句将会被执行。

### 在 switch 语句中添加默认选项

在 `switch` 语句中，你可能无法用 `case` 枚举出所有可能的值。 相反，你可以添加 `default` 语句，它会在找不到相匹配的 `case` 语句之后执行。 你可以把它看作是 `if/else` 链中最后的那个 `else` 语句。

`default` 语句应该被放到最后。

```js
switch (num) {
  case value1:
    statement1;
    break;
  case value2:
    statement2;
    break;
...
  default:
    defaultStatement;
    break;
}
```

### 在 Switch 语句添加多个相同选项

如果你忘了给 `switch` 的每一条 `case` 添加 `break`，那么后续的 `case` 会一直执行，直到遇见 `break` 为止。 如果你想为 `switch` 中的多个不同的输入设置相同的结果，可以这样写：

```js
let result = "";
switch(val) {
  case 1:
  case 2:
  case 3:
    result = "1, 2, or 3";
    break;
  case 4:
    result = "4 alone";
}
```

这样，1、2、3 都会有相同的结果。

### 用一个 Switch 语句来替代多个 if else 语句

如果你有多个选项需要选择，`switch` 语句写起来会比多个串联的 `if`/`else if` 语句容易些。 譬如:

```js
if (val === 1) {
  answer = "a";
} else if (val === 2) {
  answer = "b";
} else {
  answer = "c";
}
```

可以被下面替代：

```js
switch(val) {
  case 1:
    answer = "a";
    break;
  case 2:
    answer = "b";
    break;
  default:
    answer = "c";
}
```

## 对象obeject

### 创建 JavaScript 对象

你之前可能听过 `object` 这个词。

对象和 `arrays` 类似，区别在于数组使用索引来访问和修改数据，而对象中的数据是通过 `properties` 访问的。

对象非常适合用来存储结构化数据，可以表示真实世界中的物体，比如一只猫。

这里是一个猫对象的样本：

```js
const cat = {
  "name": "Whiskers",（这里是逗号，不是分号）
  "legs": 4,
  "tails": 1,
  "enemies": ["Water", "Dogs"]
};
```

在此示例中，所有属性都存储为字符串，例如 `name`、`legs` 和 `tails`。 然而，你也可以使用数字作为属性。 你甚至可以省略单字字符串属性中的引号，如下所示：

```js
const anotherObject = {
  make: "Ford",
  5: "five",
  "model": "focus"
};
```

然而，如果你的对象有非字符串属性的话，JavaScript 会自动将它们转为字符串。

### 通过点号表示法访问对象属性（只能用于查找对象）

和访问数组类似，访问对象属性有两种方式：点号表示法（`.`）和方括号表示法（`[]`）。

如果我们已经提前知道要访问的属性名，使用点号表示法是最方便的。

这里是一个用点符号（`.`）读取对象属性的示例：

```js
const myObj = {
  prop1: "val1",
  prop2: "val2"
};

const prop1val = myObj.prop1;
const prop2val = myObj.prop2;
```

`prop1val` 的值将为字符串 `val1`，并且`prop2val` 的值将为字符串 `val2`。

### 使用方括号表示法访问对象属性（常用，易和数组索引混淆）

访问对象属性的第二种方式是方括号表示法（`[]`）。 **如果你想访问的属性名中包含空格，就必须使用方括号表示法来获取它的属性值。**

当然，如果属性名不包含空格，也可以使用方括号表示法。

这是一个使用方括号表示法读取对象属性的例子：

```js
const myObj = {
  "Space Name": "Kirk",
  "More Space": "Spock",
  "NoSpace": "USS Enterprise"
};

myObj["Space Name"];
myObj['More Space'];
myObj["NoSpace"];
```

`myObj["Space Name"]` 将会是字符串 `Kirk`，`myObj['More Space']` 将会是字符串 `Spock`，并且`myObj["NoSpace"]` 将会是字符串 `USS Enterprise`。

**注意，如果属性名中包含空格，就必须使用引号（单引号或双引号）将它们包裹起来。**

> 方括号法比点号法实用的多，因为无论是对象属性名中含有空格还是想用变量访问对象属性，都只能用方括号法。虽然点号法更简单，但为了少出错，多使用方括号法吧。

### 通过变量访问对象属性（mid confused)

对对象上使用**方括号表示法**，还可以访问对象上作为变量值存储的属性。 当你需要遍历对象的所有属性，或者根据一个变量的值查找对应的属性值时，这种写法尤其适用。

以下是一个使用变量来访问属性的例子：

```js
const dogs = {
  Fido: "Mutt",
  Hunter: "Doberman",
  Snoopie: "Beagle"
};

const myDog = "Hunter";
const myBreed = dogs[myDog];
console.log(myBreed);
```

字符串 `Doberman` 将会出现在控制台中。

使用这一概念的另一种情况是：属性的名字是在程序运行期间动态收集得到的。如下所示：

```js
const someObj = {
  propName: "John"
};

function propPrefix(str) {
  const s = "prop";
  return s + str;
}

const someProp = propPrefix("Name");
console.log(someObj[someProp]);
```

`someProp` 的值将为字符串 `propName`，并且字符串 `John` 将会出现在控制台中。

注意，当使用变量名访问属性时，我们*没有*使用引号包裹它，因为我们正在使用的是变量的*值*，而不是变量的*名字*。

### 更新对象属性

在你创建了 JavaScript 对象后，你可以随时更新它的属性，就像更新任何其他变量那样。 你可以使用点或中括号操作符来更新。

举个例子，让我们看看 `ourDog`：

```js
const ourDog = {
  "name": "Camper",
  "legs": 4,
  "tails": 1,
  "friends": ["everything!"]
};
```

既然他是一个特别愉快的狗，让我们将他的名字更改为字符串 `Happy Camper`。 这有两种方式来更新对象的 name 属性： `ourDog.name = "Happy Camper";` 或 `ourDog["name"] = "Happy Camper";`。更新后，`ourDog.name` 的值就不再是 `Camper`，而是 `Happy Camper`。

### 给 JavaScript 对象添加新属性

你也可以像更改属性一样给 JavaScript 对象添加属性。

这里展示了如何给 `ourDog` 添加一个属性 `bark`：

```js
ourDog.bark = "bow-wow";
```

或者

```js
ourDog["bark"] = "bow-wow"
```

### 删除对象的属性

我们同样可以删除对象的属性，例如：

```js
delete ourDog.bark;
```

### 测试对象的属性

有时检查一个对象属性是否存在是非常有用的。 我们可以用对象的 `.hasOwnProperty(propname)` 方法来检查对象是否有指定的属性。 `.hasOwnProperty()` 找到该属性时返回 `true`，找不到该属性时返回 `false`。

**示例**

```js
const myObj = {
  top: "hat",
  bottom: "pants"
};

myObj.hasOwnProperty("top");
myObj.hasOwnProperty("middle");
```

第一个 `hasOwnProperty` 返回 `true`，第二个返回 `false`。

### 访问嵌套对象

我们可以通过**连续使用点号表示法和方括号表示法来访问对象的嵌套属性**。

这是一个嵌套对象：

```js
const ourStorage = {
  "desk": {
    "drawer": "stapler"
  },
  "cabinet": {
    "top drawer": { 
      "folder1": "a file",
      "folder2": "secrets"
    },
    "bottom drawer": "soda"
  }
};

ourStorage.cabinet["top drawer"].folder2;
ourStorage.desk.drawer;
```

`ourStorage.cabinet["top drawer"].folder2` 将会是字符串 `secrets`，并且 `ourStorage.desk.drawer` 将会是字符串 `stapler`。

### [实例：记录集合（very confused)](https://chinese.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/record-collection)

值得再做一遍

## 循环

### while 循环（一个条件）

 `while` 循环，当 while 指定的条件为真，循环才会执行，反之不执行。

```js
const ourArray = [];
let i = 0;

while (i < 5) {
  ourArray.push(i);
  i++;
}
```

在上面的代码里，`while` 循环执行 5 次把 0 到 4 的数字添加到 `ourArray` 数组里。

### for 循环（三个条件）

JavaScript 中最常见的循环就是 `for`，它可以循环指定次数。

for 循环中的可选三个表达式**用分号隔开**：

`for (a; b; c)`，其中`a`为初始化语句，`b`为条件语句，`c` 是最终的表达式。

**初始化语句只会在执行循环开始之前执行一次。 它通常用于定义和设置你的循环变量。**

循环条件语句会在每一轮循环的开始前执行，只要条件判断为 `true` 就会继续执行循环。 当条件为 `false` 的时候，循环将停止执行。 这意味着，如果条件在一开始就为 false，这个循环将不会执行。

**终止循环表达式在每次循环迭代结束， 在下一个条件检查之前时执行，通常用来递增或递减循环计数。**

在下面的例子中，先初始化 `i = 0`，条件 `i < 5` 为 true 时，进入循环。 每次循环后 `i` 的值增加 `1`，然后执行终止循环条件表达式 `i++`。

```js
const ourArray = [];

for (let i = 0;（分号隔开） i < 5; i++) {
  ourArray.push(i);
}
```

`ourArray` 现在的值为 `[0, 1, 2, 3, 4]`。

### do...while 循环

 `do...while` 循环，是因为不论什么情况，它都会首先 `do`（运行）循环里的第一部分代码，然后 `while`（当）规定的条件被评估为 `true`（真）的时候，它会继续运行循环。

```js
const ourArray = [];
let i = 0;

do {
  ourArray.push(i);
  i++;
} while (i < 5);
```

与while循环很像，但区别是while会先执行判断再运行循环代码，而do...while是先执行一遍循环代码再判断。

### [使用递归代替循环](https://chinese.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/replace-loops-using-recursion)（very very confused)

递归是函数调用自身的操作。 为了便于理解，有如下任务：计算数组内元素前 `n` 的元素乘积。 使用 `for` 循环， 可以这样做：

```js
  function multiply(arr, n) {
    let product = 1;
    for (let i = 0; i < n; i++) {
      product *= arr[i];
    }
    return product;
  }
```

下面是递归写法，注意代码里的 `multiply(arr, n) == multiply(arr, n - 1) * arr[n - 1]`。 这意味着可以重写 `multiply` 以调用自身而无需依赖循环。

```js
  function multiply(arr, n) {
    if (n <= 0) {
      return 1;
    } else {
      return multiply(arr, n - 1) * arr[n - 1];
    }
  }
```

递归版本的 `multiply` 详述如下。 在 base case 里，也就是 `n <= 0` 时，返回 1。 在 `n` 比 0 大的情况里，函数会调用自身，参数 n 的值为 `n - 1`。 函数以相同的方式持续调用 `multiply`，直到 `n <= 0` 为止。 所以，所有函数都可以返回，原始的 `multiply` 返回结果。

**注意：** 递归函数在没有函数调用时（在这个例子是，是当 `n <= 0` 时）必需有一个跳出结构，否则永远不会执行完毕。

> 如何理解递归，我自己的方法是，你要把function里的递归看成是一个大套娃里的小套娃，而这个套娃是一个函数，而所谓函数就是给一个输入就有一个输出。

### [实例：资料查找](https://chinese.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/profile-lookup)（very very confused)

```js
function lookUpProfile(name, prop) {
  for (let i = 0; i < contacts.length; i++) {
    if (contacts[i].firstName === name) {
        （这里我本来用的contacts[i][firstName],由于原数组是数组内嵌套对象，所以就出错了）
      if (contacts[i].hasOwnProperty(prop)) {
        return contacts[i][prop];
      } else {
        return "No such property";
      }
    } 
  } return "No such contact"
}
```

## 使用 JavaScript 生成随机分数

随机数非常适合用来创建随机行为。

在 JavaScript 中，可以用 `Math.random()` 生成一个在`0`（包括 0）到 `1`（不包括 1）之间的随机小数。 因此 `Math.random()` 可能返回 `0`，但绝不会返回 `1`。

**提示：**[使用赋值运算符存储值](https://chinese.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/storing-values-with-the-assignment-operator)这一节讲过，所有函数调用将在 `return` 执行之前结束，因此我们可以 `return`（返回）`Math.random()` 函数的值。

### 基于Math.random()生成随机整数

生成随机小数很棒，但随机数更有用的地方在于生成随机整数。

1. 用 `Math.random()` 生成一个随机小数。
2. 把这个随机小数乘以 `20`。
3. 用 `Math.floor()` 向下取整，获得它最近的整数。

记住 `Math.random()` 永远不会返回 `1`。同时因为我们是在向下取整，所以最终我们获得的结果不可能有 `20`。 这确保了我们获得了一个在 `0` 到 `19` 之间的整数。

把操作连缀起来，代码类似于下面：

```js
Math.floor(Math.random() * 20);
```

我们先调用 `Math.random()`，把它的结果乘以 20，然后把上一步的结果传给 `Math.floor()`，最终通过向下取整获得最近的整数。

### 基于Math.random()生成某个范围内的随机整数

我们之前生成的随机数是在 0 到某个数之间，现在我们要生成的随机数是在两个指定的数之间。

我们需要定义一个最小值 `min` 和一个最大值 `max`。

下面是我们将要使用的方法， 仔细看看并尝试理解这行代码到底在干嘛：

```js
Math.floor(Math.random() * (max - min + 1)) + min
```

## 使用 parseInt 函数

`parseInt()` 函数解析一个字符串返回一个整数。 下面是一个示例：

```js
const a = parseInt("007");
```

上述函数将字符串 `007` 转换为整数 `7`。 如果字符串中的第一个字符不能转换为数字，则返回 `NaN`。

### 使用 parseInt 函数并传入一个基数

`parseInt()` 函数解析一个字符串并返回一个整数。 它还可以传入第二个参数，指定了字符串中数字的基数。 基数可以是 2 到 36 之间的整数。

函数调用如下所示：

```js
parseInt(string, radix);
```

这是一个示例：

```js
const a = parseInt("11", 2);
```

变量 radix 表示 `11` 是在二进制系统中。 这个示例将字符串 `11` 转换为整数 `3`。

## 用三元运算符（简化if）

条件运算符（ conditional operator,）（也称为三元运算符（ ternary operator））的就像写成一行的 if-else 表达式

语法是：`a ? b : c`, where `a` 是条件，当条件返回 `true` 的时候运行代码 `b`，当条件返回 `false` 的时候运行代码 `c`。

以下函数使用 `if/else` 语句来检查条件：

```js
function findGreater(a, b) {
  if(a > b) {
    return "a is greater";
  }
  else {
    return "b is greater or equal";
  }
}
```

这可以使用三元运算符重写：

```js
function findGreater(a, b) {
  return a > b ? "a is greater" : "b is greater or equal";
}
```

### 三元运算符嵌套

下面的函数使用 `if`，`else if` 和 `else` 语句来检查多个条件：

```js
function findGreaterOrEqual(a, b) {
  if (a === b) {
    return "a and b are equal";
  }
  else if (a > b) {
    return "a is greater";
  }
  else {
    return "b is greater";
  }
}
```

以上函数可以使用多个三元运算符重写：

```js
function findGreaterOrEqual(a, b) {
  return (a === b) ? "a and b are equal" 
    : (a > b) ? "a is greater" 
    : "b is greater";
}
```

如上文所示，**对多个三元运算符进行每个条件都是单独一行的格式化被认为是最佳做法。** 使用多个三元运算符而没有适当的缩进可能会使您的代码难以理解。 例如：

```js
function findGreaterOrEqual(a, b) {
  return (a === b) ? "a and b are equal" : (a > b) ? "a is greater" : "b is greater";
}
```

## [使用递归创建一个倒计时](https://chinese.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/use-recursion-to-create-a-countdown)

这里你可能会疑惑，为什么n从大到小递归，数组却是从小到大添加。

你可以把要递归的函数全部展开，这样就是五个函数套娃，先算最里面的套娃，并push给那个声明的变量，最外面的套娃反而是最后被push的，所以就反过来了。

## [使用递归来创建一个数字序列](https://chinese.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/use-recursion-to-create-a-range-of-numbers)
