## [CSS书写顺序](https://www.shejidaren.com/css-written-specifications.html)

1.位置属性(position, top, right, z-index, display, float等)
2.大小(width, height, padding, margin)
3.文字系列(font, line-height, letter-spacing, color- text-align等)
4.背景(background, border等)
5.其他(animation, transition等)

![css-written-order](http://images.shejidaren.com/wp-content/uploads/2013/09/css-written-order.png)

## [CSS每个选择器的属性书写顺序](https://zhuanlan.zhihu.com/p/433893912)（重要）

从生活中看一个人是否有气质，要看他的穿着打扮。

在编程中看一个人是否有能力，要看他的：规范标准、优雅高质量等等

建议遵循以下顺序：

1. **布局定位属性**：display / position / float / clear / visibility / overflow（建议 display 第一个写，毕竟关系到模式）
2. **自身属性**：width / height / margin / padding / border / background
3. **文本属性**：color / font / text-decoration / text-align / vertical-align / white- space / break-word
4. **其他属性（CSS3）**：content / cursor / border-radius / box-shadow / text-shadow / background:linear-gradient …

举例：

```css
.jdc {
    display: block;
    position: relative;
    float: left;
    width: 100px;
    height: 100px;
    margin: 0 10px;
    padding: 20px 0;
    font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
    color: #333;
    background: rgba(0,0,0,.5);
    border-radius: 10px;
 } 
```

按照上述1 2 3 4 5的顺序进行书写。

目的：减少浏览器reflow（回流），提升浏览器渲染dom的性能

原理：浏览器的渲染流程为——

①解析html构建dom树，解析css构建css树：将html解析成树形的数据结构，将css解析成树形的数据结构

②构建render树：DOM树和CSS树合并之后形成的render树。

③布局render树：有了render树，浏览器已经知道那些网页中有哪些节点，各个节点的css定义和以及它们的从属关系，从而计算出每个节点在屏幕中的位置。

④绘制render树：按照计算出来的规则，通过显卡把内容画在屏幕上。

css样式解析到显示至浏览器屏幕上就发生在②③④步骤，可见浏览器并不是一获取到css样式就立马开始解析而是根据css样式的书写顺序将之按照dom树的结构分布render样式，完成第②步，然后开始遍历每个树结点的css样式进行解析，此时的css样式的遍历顺序完全是按照之前的书写顺序。在解析过程中，一旦浏览器发现某个元素的定位变化影响布局，则需要倒回去重新渲染正如按照这样的书写书序：

width: 100px;

height: 100px;

background-color: red ;

position: absolute;

当浏览器解析到position的时候突然发现该元素是绝对定位元素需要脱离文档流，而之前却是按照普通元素进行解析的，所以不得不重新渲染，解除该元素在文档中所占位置，然而由于该元素的占位发生变化，其他元素也可能会受到它回流的影响而重新排位。最终导致③步骤花费的时间太久而影响到④步骤的显示，影响了用户体验。

所以规范的的css书写顺序对于文档渲染来说一定是事半功倍的！
————————————————
版权声明：本文为CSDN博主「QPQ_」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/qq_36060786/article/details/79311244

<details> <summary>show/hidden</summary>
<pre>扩展：还有一个会影响浏览器渲染性能的词汇“repaint（重绘）
repaint（重绘）：改变某个元素的背景色、文字颜色、边框颜色等等不影响它周围或内部布局的属性时，屏幕的一部分要重画，但是元素的几何尺寸没有变。
注意：
a.render树的结构不等同于DOM树的结构，一些设置display:none的节点不会被放在render树中，但会在dom树中。
b.有些情况，比如修改了元素的样式，浏览器并不会理科reflow或repaint，而是把这些操作积攒一批，然后做一次reflow，这也叫做异步reflow.但在有些情况下，比如改变窗口，改变页面默认的字体等，对 于这些情况，浏览器会马上进行reflow.
c.为了更好的用户体验，渲染引擎将会尽可能早的将内容呈现到屏幕上，并不会等到所有的html都解析完成之后再去构建和布局render树。它是解析完一部分内容就显示一部分内容，同时，可能还在通过网络下载其余内容。
此文部分是参考博客园
https://mp.weixin.qq.com/s?__biz=MzA3NTIwOTYxMw==&mid=2447522046&idx=1&sn=754ba20d30a93bb754013d59d001ef44&chksm=8b62961cbc151f0abb932aa218e00ddc512e0f9179e3e83b6f4af87ccd8dd6e679fa75b9aba8&mpshare=1&scene=23&srcid=0120TWyG73R2Ur6y1e2McZWc#rd
————————————————
版权声明：本文为CSDN博主「QPQ_」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。</pre>
</details>

### [常用的CSS命名规则](https://www.cnblogs.com/ipoplar/p/4539415.html)

头：header　　内容：content/container　　尾：footer　　导航：nav　　侧栏：sidebar

栏目：column　　页面外围控制整体布局宽度：wrapper　　左右中：left right center

登录条：loginbar　　标志：logo　　广告：banner　　页面主体：main　　热点：hot

新闻：news　　下载：download　　子导航：subnav　　菜单：menu

子菜单：submenu　　搜索：search　　友情链接：friendlink　　页脚：footer

版权：copyright　　滚动：scroll　　内容：content　　标签页：tab

文章列表：list　　提示信息：msg　　小技巧：tips　　栏目标题：title

加入：joinus　　指南：guild　　服务：service　　注册：regsiter

状态：status　　投票：vote　　合作伙伴：partner