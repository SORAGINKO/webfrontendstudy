<!-- vscode-markdown-toc -->
* 1. [Preparation](#Preparation)
* 2. [最基本的选择器id&class](#idclass)
* 3. [创建css](#css)
	* 3.1. [CSS变量](#CSS)
* 4. [背景](#)
* 5. [文本，字体，段落属性](#-1)
* 6. [链接](#-1)
* 7. [列表和表格](#-1)
* 8. [盒模型](#-1)
	* 8.1. [border(边框)](#border)
		* 8.1.1. [outline轮廓](#outline)
		* 8.1.2. [margin外边距](#margin)
		* 8.1.3. [padding填充](#padding)
	* 8.2. [尺寸dimesion显示display和定位position](#dimesiondisplayposition)
		* 8.2.1. [dimesion](#dimesion)
		* 8.2.2. [display](#display)
		* 8.2.3. [position](#position)
* 9. [CSS实现居中的N种方式](#CSSN)
	* 9.1. [水平居中的方式](#-1)
		* 9.1.1. [文本居中](#-1)
		* 9.1.2. [非文本元素居中](#-1)
		* 9.1.3. [利用弹性盒子居中](#-1)
	* 9.2. [垂直居中](#-1)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->[toc]

来源：菜鸟教程&freecodecamp

##  1. <a name='Preparation'></a>Preparation

CSS是由一个个的选择器组成的，选择器是这样的

```
selector {		/*selector和{之间必须有空格*/
	key: value; /*这个部分也叫声名*/
	key: value; /*一般的值不用带引号*/
}
```

##  2. <a name='idclass'></a>最基本的选择器id&class

id选择器是#开头的，id属性在一个html文档中是唯一的。
元素可以有多个class，用空格隔开就可以，如<img class="class1 class2">

class选择器是`.`开头的，class属性不唯一。所以class选择器也常和元素一起使用，如p.class就是选择的p元素里class属性为class的元素。

##  3. <a name='css'></a>创建css

**（内联样式）Inline style > （内部样式）Internal style sheet >（外部样式）External style sheet > 浏览器默认样式**

内联：由于要将表现和内容混杂在一起，内联样式会损失掉样式表的许多优势。请慎用这种方法，例如当样式仅需要在一个元素上应用一次时。

`<p style="color:sienna;margin-left:20px">这是一个段落。</p>`

内部CSS：

```
<head>
<style>
hr {color:sienna;}
p {margin-left:20px;}
body {background-image:url("images/back40.gif");}
</style>
</head>
```

**外部**CSS：

`<head> <link rel="stylesheet" type="text/css" href="mystyle.css"> </head>`

###  3.1. <a name='CSS'></a>CSS变量

在CSS内用 --key: value;创建CSS内的变量。如`--backcolor: grenn;`
用var来用这个变量。如` background-color: var(backcolor);`如果变量还没有设置，可以在backcolor后加一个备用值，但要用，和空格隔开。如`background-color: var(backcolor, black)

##  4. <a name=''></a>背景

- background-color

  > 颜色值通常以以下方式定义:
  >
  > - 十六进制 - 如："#ff0000"
  > - RGB - 如："rgb(255,0,0)"
  > - 颜色名称 - 如："red"

- background-image

> 例：body {background-image:url('paper.gif');}

- background-repeat

> 例：body {
> background-image:url('gradient2.png');
> background-repeat:**repeat-x/repeat-y/no-repeat**;
> }

- background-attachment:"value" 设置背景图像是否固定或者随着页面的其余部分滚动。

  > scroll	背景图片随着页面的滚动而滚动，这是默认的。
  > fixed	背景图片不会随着页面的滚动而滚动。
  > local	背景图片会随着元素内容的滚动而滚动。
  > initial	设置该属性的默认值。 阅读关于 initial 内容
  > **inherit	指定 background-attachment 的设置应该从父元素继承。** 

- background-position:设置背景图像的起始位置。

> left top
> left center
> left bottom
> right top
> right center
> right bottom
> center top
> center center
> center bottom	
>
> 如果仅指定一个关键字，其他值将会是"center"

为了简化这些属性的代码，我们可以将这些属性合并在同一个属性中.
背景颜色的简写属性为 "background":

> 实例
> body {background:#ffffff url('img_tree.png') no-repeat right top;}
>
> 当使用简写属性时，属性值的顺序为：:
>
> - background-color
> - background-image
> - background-repeat
> - background-attachment
> - background-position
>
> 以上属性无需全部使用，你可以按照页面的实际需要使用.

##  5. <a name='-1'></a>文本，字体，段落属性

* "color:颜色名or颜色值"
* textalign:value"

> 文本可居中或对齐到左或右,两端对齐。 center/left/right
>
> 当text-align设置为"justify"，每一行被展开为宽度相等，左，右外边距是对齐（如杂志和报纸）。

* text-decoration:“none"   none属性最常用

> 从设计的角度看 text-decoration属性主要是用来删除链接的下划线

* text-tranform:"value"

> 可用于所有字句变成大写或小写字母，或每个单词的首字母大写。
>
> uppercase/lowercase/capitalize

* text-indent:50px;

文本缩进属性是用来指定文本的第一行的缩进。上述例子是首行缩进50px。

***

* font-family 	属性设置文本的字体系列。如果字体名字超过2个word或字，要用引号。如font-family:"宋体“; font-family:"Time New Rome";
* font-style:"normal/italic/oblique"  主要是用于指定斜体文字的字体样式属性
* 字体大小
> 能否管理文字的大小，在网页设计中是非常重要的。但是，你不能通过调整字体大小使段落看上去像标题，或者使标题看上去像段落。
> 设置字体大小像素 font-size="__px"
> 用em来设置大小 font-size="__em"
> 用百分比来设置大小 font-size="__%"

***

line-height: __px; 行间距


##  6. <a name='-1'></a>链接

链接的样式，可以用任何CSS属性（如颜色，字体，背景等）。
特别的链接，可以有不同的样式，这取决于他们是什么状态。
这四个链接状态是：

- a:link - 正常，未访问过的链接
- a:visited - 用户已访问过的链接
- a:hover - 当用户鼠标放在链接上时
- a:active - 链接被点击的那一刻

默认a:visited为蓝色，a:hover为红色下划线。

**当设置为若干链路状态的样式，也有一些顺序规则：**

- **a:hover 必须跟在 a:link 和 a:visited后面**
- **a:active 必须跟在 a:hover后面**

带有红色下划线的链接是很丑的，所以常用

> a:link {text-decoration:none;} 
> a:visited {text-decoration:none;} 
> a:hover {text-decoration:underline;} 
> a:active {text-decoration:underline;}

##  7. <a name='-1'></a>列表和表格

list-style-type属性指定列表项标记的类型

> ul.a {list-style-type: circle;}
> ul.b {list-style-type: square;}
>
> ol.c {list-style-type: upper-roman;}
> ol.d {list-style-type: lower-alpha;}

ist-style-image: url('sqpurple.gif');属性指定列表标记项的图像

列表简写属性list-style:"value";

可以按顺序设置如下属性：

- list-style-type
- list-style-position (有关说明，请参见下面的CSS属性表)
- list-style-image

如果上述值丢失一个，其余仍在指定的顺序，就没关系。

***

* 表格边框，使用border属性。如

table, th, td { border: 1px solid black; }

* border-collapse 属性设置表格的边框是否被折叠成一个单一的边框或隔开
* Width和height属性定义表格的宽度和高度。可以用px，em，%
* text-align属性设置水平对齐方式，向左，右，或中心：text-align:right;
* 垂直对齐属性设置垂直对齐，比如顶部，底部或中间：vertical-align:bottom;
* padding:15px; 表格内边距
* 颜色和背景色

background-color:green;    
color:white;

##  8. <a name='-1'></a>盒模型

###  8.1. <a name='border'></a>border(边框)

border-color: "colorname/colorvalue"
border-width: "__px"
border-style: "solid/inset/dotted"

> `border-style` 默认值是 `none`，这意味着如果您只修改 [`border-width`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-width) 和 [`border-color`](https://developer.mozilla.org/zh-CN/docs/Web/CSS/border-color) 是不会出现边框的。

border-radius: "px/%" 来让边框的四角变得圆润。如果设成50%就会变成圆形。

####  8.1.1. <a name='outline'></a>outline轮廓

####  8.1.2. <a name='margin'></a>margin外边距

####  8.1.3. <a name='padding'></a>padding填充



###  8.2. <a name='dimesiondisplayposition'></a>尺寸dimesion显示display和定位position

####  8.2.1. <a name='dimesion'></a>dimesion

####  8.2.2. <a name='display'></a>display

diplay值有inline，block，这两个好理解。还有个inline-block，不好理解

####  8.2.3. <a name='position'></a>position

position值有
static，应该也是元素的默认属性，此属性下top right left bottom都不可用；
relative，相对定位，相对于static来偏移;
absolute，绝对定位，相对于父元素来偏移；
fixed，相对于浏览器窗口来偏移。

此外，还有一个不是position的值的属性float也可以很方便的用于定位
float值有none（默认值）left right。
但要注意设计好元素的width属性，因为如果多个浮动元素的总宽超过了显示屏幕的最大宽度，肯定浮不上去了。
所以最好用百分数来设定width，因为首先你不知道当前屏幕的宽度，其次跨平台也方便。
float的默认浮动方式是将文档流中的元素以从左到右的方式从第一行排列。
可以用**属性**float-direction（值有row，cloumn，row-reverse，column-reverse）来修改排列方式。（可见，float-direction的默认值是row）

##  9. <a name='CSSN'></a>CSS实现居中的N种方式

[[CSS实现居中的方式](https://www.cnblogs.com/ghq120/p/10939835.html)]
###  9.1. <a name='-1'></a>水平居中的方式
####  9.1.1. <a name='-1'></a>文本居中

css中的text-align: ceter;

####  9.1.2. <a name='-1'></a>非文本元素居中

设置display为block或inline-block后将margin设为auto

####  9.1.3. <a name='-1'></a>利用弹性盒子居中

###  9.2. <a name='-1'></a>垂直居中
