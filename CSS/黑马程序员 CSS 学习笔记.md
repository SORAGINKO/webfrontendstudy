# ***\*1. CSS 简介\****



CSS 的主要使用场景就是美化网页，布局页面的

## 1.1 HTML 的局限性

说起 HTML ，他***\*只关注内容的语义\****。比如 <h1> 表明这是一个大标题，<p> 表明这是一个段落，<img> 表明这有一个图片，<a> 表示此处有链接。

虽然 HTML 可以做简单的样式，但是带来的是无尽的***\*臃肿和繁琐\****

## 1.2 CSS-网页的美容师

***\*CSS\**** 是***\*层叠样式表\****（***\*Cascading Style Sheets\****）的简称

有时我们也称之为 ***\*CSS 样式表或级联样式表\****

CSS 也是一种标记语言

CSS主要用于设置 HTML 页面中的***\*文本内容\****（字体，大小，对齐方式等）、***\*图片的外形\****（宽高、边框样式、边距等）以及***\*版面的布局和外观显示样式\****

CSS 让我们的网页更加丰富多彩，布局更加灵活自如

简单理解：***\*CSS 可以美化 HTML，让 HTML 更漂亮，让页面布局更简单\****

**总结：**

\1. HTML 主要做结构，显示元素内容

\2. CSS 美化 HTML，布局网页

**3. CSS 最大价值：由 HTML 专注去做结构呈现，样式交给 CSS，即结构（HTML）与样式（CSS）相分离。**

## 1.3 CSS 语法规范

使用 HTML 时，需要遵从一定的规范，CSS 也是如此，要想熟练地使用 CSS 对网页进行修饰，首先需要了解 CSS 样式规则

***\*CSS 规则由两个主要的部分构成：选择器以及一条或多条声明\****



![img](https://img-blog.csdnimg.cn/6501b4a9b787490aa095a011587bb7cd.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_8,color_FFFFFF,t_70,g_se,x_16)

-  **选择器**是用于指定 CSS 样式的 ***\*HTML 标签\****，花括号内是对该对象设置的具体样式
-  属性和属性值以“键值对”的形式出现
-  属性是对指定的对象设置的样式属性，例如字体大小，文本颜色等
-  属性和属性值之间用英文“ ***\*:\**** ”分开
-  多个“键值对”之间用英文“ **;** ”进行区分

## 1.4 CSS 代码风格

以下代码书写风格不是强制规范，而是符合实际开发书写方式

### 1. 样式格式书写

① 紧凑格式

```css
h3 { color: deeppink;font-size: 20px;}
```

② 展开格式

```css
h3 {



    color: pink;



    font-size: 20px;



}
```

哪个看的舒服一目了然吧

### 2. 样式大小写

```css
h3 {



    color: pink;



}
H3 {



    COLOR: PINK;



}
```

建议都使用小写，特殊情况除外

\3. 空格规范

```css
h3 {



    color: pink;



}
```

① 属性值前面，冒号后面，保留一个空格

② 选择器（标签）和大括号中间保留空格

# 2. CSS 基础选择器

## 2.1 CSS 选择器的作用

选择器（选择符）就是根据不同需求把不同的标签选出来这就是选择器的作用

简答来说，就是***\*选择标签用的\****

![img](https://img-blog.csdnimg.cn/6501b4a9b787490aa095a011587bb7cd.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_8,color_FFFFFF,t_70,g_se,x_16)

以上 CSS 做了两件事：

\1. 找到所有 h1 标签，选择器（选对人）

\2. 设置这些标签的样式，比如颜色为红色（做对事）

## 2.2 选择器分类

**选择器**分为**基础选择器和复合选择器**两个大类，我们这里先讲解一下基础选择器

- 基础选择器是由***\*单个选择器组成\****的
- 基础选择器又包括：***\*标签选择器，类选择器，id选择器和通配符选择器\****

## 2.3 标签选择器

***\*标签选择器\****（元素选择器）是指用 ***\*HTML 标签名称\****作为选择器，按标签名称分类，为页面中某一类标签指定统一的 CSS 样式

**语法**

> 标签名{
>
> ​    属性1：属性值1；
>
> ​    属性2：属性值2；
>
> ​    属性3：属性值3；
>
> ​    ...
>
> }

**作用**

标签选择器可以把某一类标签全部选择出来，比如所有的 <div> 标签和所有的 <span> 标签

**优点**

能快速为页面中同类型的标签统一设置样式

**缺点**

不能设计差异化样式，只能选择全部的当前标签

## 2.4 类选择器

如果想要差异化选择不同的标签，单独选一个或几个标签，可以使用**类选择器**

类选择器在 HTML 中以 class 属性表示，在 CSS 中，类选择器以一个点“.”号表示

**语法**

> .类名 {
>
> ​    属性1: 属性值1;
>
> ​    ...
>
> }

```css
.red{



    color: red;



}
```

结构需要用 class 属性来调用 class 类的意思

```html
<div class='red'> 变红色 </div>
```

**注意**

① 类选择器使用“.”*（英文点号）进行标识，后面紧跟类名（自定义，我们自己命名的）

② 可以理解为给这个标签起了一个名字，来表示

③ 长名称或词组可以使用中横线来为选择器命名

④ 不要使用纯数字、中文等命名，尽量使用英文字母来表示

⑤ 命名要有意义，尽量使别人一眼就知道这个类名的目的

## 案例

利用类选择器画三个盒子

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>利用类选择器画三个盒子</title>



    <style>



        .red {



            width: 100px;



            height: 100px;



            background-color: red;



        }



        .green{



            width: 100px;



            height: 100px;



            background-color: green;



        }



    </style>



</head>



<body>



    <p>有点意思</p>



    <div class="red">这是红色</div>



    <div class="green">这是绿色</div>



    <div class="red">这又是红色</div>



</body>



</html>
```

![img](https://img-blog.csdnimg.cn/2b3b8289ea8441c09def16a29dc141a5.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_11,color_FFFFFF,t_70,g_se,x_16)

## 2.4 类选择器-多类名

我们可以给一个标签指定多个类名，从而达到更多的选择目的。这些类名都可以选出这个标签

简单说就是一个标签有多个名字

### 1. 多类名使用方式

```html
<div class="red font20">亚瑟</div>
```

① 在标签 class 属性中写 多个类名

② 多个类名中间必须用空格分开

③ 这个标签就可以分别具有这些类名的样式

### 2. 多类名开发中使用场景

① 可以把一些标签元素相同的样式（共同的部分）放到一个类里面

② 这些标签都可以调用这个公共的类，然后再调用自己独有的类

③ 从而节省 CSS 代码，统一修改也非常方便

- 各个类名中间用空格隔开
- 简单理解：就是给某个标签添加了多个类，或者这个标签有多个名字
- 这个标签就可以分别具有这些类名的样式
- 从而节省 CSS 代码，统一修改也非常方便
- 多类名选择器在后期布局比较复杂的情况下，还是较多使用的

## 2.5 id 选择器

id 选择器可以为标有特定 id 的 HTML 元素指定特定的样式

HTML 元素以 ***\*id 属性\****来设置 id 选择器，CSS 中 id 选择器以 “**#**” 来定义

### 语法

```css
#id名{



    属性1：属性值1；



    ...



}
```

例如，将 id 为 nav 元素中的内容设置为红色

```css
#nav{



    color:red;



}
```

id 选择器和类选择器的区别

1.  类选择器（class）像是人名，一个人可以有多个名字，一个名字也可以被多个人使用
2.  id 选择器像是身份证号码，每个人独一无二，不能重复
3.  id 选择器和类选择器最大的不同在于使用次数上
4.  类选择器在修改样式中用的最多，id 选择器一般用于页面唯一性的元素经常和 JS 搭配使用

## 2.6 通配符选择器

在 CSS 中，通配符选择器使用 “*****” 定义，它表示选取页面中所有元素（标签）。

### 语法

```css
*{



    属性1:属性值1;



    ...



}
```

- 通配符选择器不需要调用，自动就给所有的元素使用样式
- 特殊情况才使用，后面讲解使用场景（以下是清楚所有的元素标签的内外边距）

```css
*{



    margin:0;



    padding:0;



}
```

## 2.7 基础选择器总结

![img](https://img-blog.csdnimg.cn/3732736b8c96473aad214cbd10e22ad4.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_20,color_FFFFFF,t_70,g_se,x_16)

-  每个基础选择器都有使用场景，都需要掌握
-  如果是修改样式，类选择器是使用最多的

# ***\*3. CSS 字体属性\****

CSS Fonts (字体)属性用于定义**字体系列**、大小、粗细和文本样式(如斜体)。

## 3.1 字体系列

CSS 使用 ***\*font-family\**** 属性定义文本的字体系列

```css
p { font-family:"微软雅黑";}



div (font-family:Arial,"Microsoft Yahei","微软雅黑";)
```

- 各种字体之间必须使用英文状态下的逗号隔开
- 一般情况下，如果有空隔开的多个单词组成的字体，加引号
- 尽量使用系统默认自带字体，保证在任何用户的浏览器中都能正确显示
- 最常见的几个字体：body{font-family:'Microsoft YaHei','tahoma,arial','Hiragino Sans GB';}

## 3.2 字体大小

CSS 使用 font-size 属性定义字体大小

```css
p {



    font-size: 20px;



}
```

- px(像素)大小是我们网页的最常用的单位
- 谷歌浏览器默认的文字大小为16px
- 不同浏览器可能默认显示的字号大小不一致，我们尽量给一个明确值大小，不要默认大小
- 可以给 body 指定整个页面文字的大小

## 3.3 字体粗细

CSS 使用 **font-weight** 属性设置文本字体的粗细

```css
p {



    font-weight: bold;



}
```

| **属性值** | **描述**                                                    |
| ---------- | ----------------------------------------------------------- |
| normal     | 默认值（不加粗）                                            |
| bold       | 定义粗体（加粗的）                                          |
| 100-900    | 400等同于 normal，而 700 等同于 bold 注意这个数字后面没单位 |

学会让加粗标签（比如 h 和 strong 等）不加粗，或者其他标签加粗，实际开发时，更常用数字

## 3.4 文字样式

CSS 使用 **font-style** 属性设置文本的风格

```css
p{



    font-style: normal;



}
```

| **属性值** | **作用**                                               |
| ---------- | ------------------------------------------------------ |
| normal     | 默认值，浏览器会显示标准的字体样式 font-style: normal; |
| italic     | 浏览器会显示斜体的字体样式                             |

**注意：平时我们很少给文字加斜体，反而要给斜体标签（em，i）改为不倾斜字体。**

## 3.5 字体复合属性

字体属性可以把以上文字样式综合来写，这样可以更节约代码：

```css
body{



    font: font-style    font-weight    font-size/line-height    font-family;



}
```

- 使用 font 属性时，必须按上面语法格式中的顺序书写，***\*不能更换顺序\****，并且各个属性间用***\*空格\****隔开
- 不需要设置的属性可以省略，但**必须保留 font-size 和 font-family 属性**，否则 font 属性将不起作用

# ***\*4. CSS 文本属性\****

CSS Text（文本）属性可定义文本的***\*外观\****，比如文本的颜色，对齐文本，装饰文本，文本缩进，行间距等

## 4.1 文本颜色

**color** 属性用于定义文本的颜色

```css
div{



    color: red;



}
```

| **表示**       | **属性值**                    |
| -------------- | ----------------------------- |
| 预定义的颜色值 | red，green，blue，pink        |
| 十六进制       | #FF0000，#FF6600，#29D794     |
| RGB代码        | rgb(255,0,0)或rgb(100%,0%,0%) |

***\*开发中最常用的是十六进制\****

## 4.2 对齐文本

**text-align** 属性用于设置元素内文本内容的水平对齐方式

```css
div{



    text-align: center;



}
```

| **属性值** | **解释**         |
| ---------- | ---------------- |
| left       | 左对齐（默认值） |
| right      | 右对齐           |
| center     | 居中对齐         |

## 4.3 装饰文本

***\*text-decorating\**** 属性规定添加到文本的修饰，可以给文本添加下划线，删除线，上划线等。

```css
div{



    text-decoration: underline;



}
```

| **属性值**   | **描述**                 |
| ------------ | ------------------------ |
| none         | 默认 没有装饰线          |
| underline    | 下划线 链接 a 自带下划线 |
| overline     | 上划线                   |
| line-through | 删除线                   |

## 4.4 文本缩进

***\*text-indent\**** 属性用来指定文本的第一行的缩进，通常是将**段落的首行缩进**。

```css
div{



    text-indent: 10px;



}
```

通过设置该属性，所有元素的第一行都可以缩进一个给定的长度，甚至该长度可以是负值

```css
p{



    text-indent: 2em;



}
```

***\*em\**** 是一个相对单位，就是当前元素（font-size）***\*1个文字的大小\****，如果当前元素没有设置大小，则会按照父元素的一个文字大小

## 4.5 行间距

***\*line-height\**** 属性用于设置行间的距离（行高）。可以控制文字行与行之间的距离

```css
p{



    line-height: 26px;



}
```

![img](https://img-blog.csdnimg.cn/d070c1d11f08486091f14d4b1fed2d76.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_20,color_FFFFFF,t_70,g_se,x_16)

# ***\*5. CSS 引入方式\****

## 5.1 CSS 的三种[样式表](https://so.csdn.net/so/search?q=样式表&spm=1001.2101.3001.7020)

按照 CSS 样式书写的位置（或者引入的方式），CSS 样式表可以分为三大类：

\1. 行内样式表（行内式）

\2. 内部样式表（嵌入式）

\3. 外部样式表（链接式）

## 5.2 行内样式表

行内样式表（内联样式表）是**在元素标签内部的 style 属性中设定 CSS 样式**适合于修改简单样式

```html
<div style="color: red;font-size: 12px">青春不常在，抓紧谈恋爱</div>
```

- style 其实就是标签的属性
- 在双引号中间，写法要符合 CSS 规范
- 可以控制当前的标签设置样式
- 由于书写繁琐，并且没有体现出结构与样式相分离的思想，所以不推荐大量使用，只有对当前元素添加简单样式的时候，可以考虑使用
- 使用行内样式表设定 CSS，通常也被称为**行内式引入**

## 5.3 内部样式表

内部样式表（内嵌样式表）是写到 HTML 页面内部。是将所有的 CSS 代码抽取出来，单独放到一个 <style> 标签中。

```html
<style>



    div{



        color: red;



        font-size: 12px;



    }



</style>
```

- <style> 标签理论上可以放在 HTML 文档的任何地方，但一般会放在文档的 <head> 标签中
- 通过此种方式，可以方便控制当前整个页面中的元素样式设置
- 代码结构清晰，但是并没有实现结构与样式完全分离
- 使用内部样式表设定 CSS，通常也称为**嵌入式引入**，这种方式是我们时常用的方式

## 5.4 外部样式表

实际开发都是外部样式表，适合于样式比较多的情况，核心是：样式单独写到 CSS 文件中，之后把 CSS 文件引入到 HTML 页面中使用

引入外部样式表分为两步：

\1. 新建一个后缀名为 .css 的样式文件，把所有 CSS 代码都放入此文件中

\2. 在 HTML 页面中，使用 <link> 标签引入这个文件

```html
<link rel="stylesheet" href="css文件路径">
```

| **属性** | **作用**                                                     |
| -------- | ------------------------------------------------------------ |
| rel      | 定义当前文档与被链接文档之间的关系在这里需要指定为"stylesheet"表示被链接的文档是一个样式表文件 |
| href     | 定义所链接外部样式表文件的URL可以是相对路径，也可以是绝对路径 |

使用外部样式表设定 CSS，通常也被称为**外链式或链接式引入**，这种方式是开发中常用的方式

# ***\*6. Chrome 调试工具\****

Chrome 浏览器提供了一个非常好用的调试工具，可以用来调试我们的 HTML 结构和 CSS 样式

## 6.1 打开调试工具

打开 Chrome 浏览器，按下 **F12** 键或者**右击页面空白处→检查**

## 6.2 使用调试工具

- Ctrl + 滚轮 可以放大开发者工具代码大小
- 左边是 HTML 元素结构，右边是 CSS 样式
- 右边 CSS 样式可以改动数值（左右箭头或者直接输入）和查看颜色
- Ctrl + 0 复原浏览器大小
- 如果点击元素发现右侧没有样式引入，极有可能是类名或者样式引入错误。
- 如果有样式，但是样式前面有黄色叹号提示，则是样式属性书写错误

# ***\*7. Emmet 语法\****

Emmet 语法的前身是 Zen coding，它使用缩写，来提高 html/css 的编写速度，vscode已内部集成

\1. 快速生成 HTML 结构语法

\2. 快速生成 CSS 样式语法

## **7.1 快速生成 HTML 结构语法**

\1. 生成标签 直接输入标签名 按 tab键 即可 比如 div 然后 tab键，就可以生成 <div></div>

\2. 如果想要生成多个相同标签，加上 * 就可以了 比如 div*3 就可以快速生成3个div

\3. 如果有父子级关系的标签，可以用 > 比如 ul > li 就可以了

\4. 如果有兄弟关系的标签，用 + 就可以了 比如 div + p

\5. 如果生成带有类名或者 id 名字的，直接写 .demo 或者 #two tab键 就可以了

\6. 如果生成的 div 类名是有顺序的，可以用 自增符号 $

\7. 如果想要在生成的标签内部写内容可以用 { } 表示

## 7.2 快速生成 CSS 样式语法

CSS 基本采取简写形式即可

\1. 比如 w200 按 tab 可以生成 width: 200px;

\2. 比如 lh26 按 tab 可以生成 line-height: 26px;

7.3 快速格式化代码

vscode 快速格式化代码：shift + alt + f

也可以设置 当我们 保存页面的时候自动格式化代码：

1）文件——>【首选项】——>【设置】；

2）搜索 emmet.include;

3）在 setting.json 下的【用户】中添加以下语句：

"editor.formatOnType":true

"editor.formatOnSave":true

只需要设置一次即可，以后都可以自动保存格式化代码

# ***\*8. CSS 的复合选择器\****

## 8.1 什么是复合选择器

在 CSS 中，可以根据选择器的类型把选择器分为基础选择器和复合选择器，复合选择器是建立在基础选择器之上，对基本选择器进行组合形成的

- 复合选择器可以更准确、更高效的选择目标元素（标签）
- 复合选择器是由两个或多个基础选择器，通过不同的方式组合而成的
- 常用的复合选择器包括：后代选择器、子选择器、并集选择器、伪装选择器等

## 8.2 后代选择器（重要）

**后代选择器**又称为**包含选择器**，可以选择父元素里面子元素。其写法就是把外层标签写在前面，内层标签写在后面，中间用空格分隔，当标签发生嵌套时，内层标签就成为外层标签的后代。

语法：

> 元素1 元素2 （样式声明）

上述语法表示**选择元素 1 里面的所有元素 2**（后代元素）

例如：

```css
ul li { 样式声明 } /* 选择 ul 里面所有的 li 标签元素 */
```

- 元素 1 和 元素 2 中间用**空格隔开**
- 元素 1 是父级，元素 2 是 子级，最终选择的是***\*元素 2\****
- 元素 2 可以是儿子，也可以是孙子等，只要是元素 1 的后代即可
- 元素 1 和 元素 2 可以是任意基础选择器

## 8.3 子代选择器（重要）

**子元素选择器（子选择器）**只能选择作为某元素的最近一级子元素，简单理解就是选亲儿子元素

语法：

> 元素 1 > 元素 2 （样式声明）

上述语法表示**选择元素 1 里面的所有直接给后代（子元素）元素2**。

例如：

```css
div > p {样式声明} /* 选择 div 里面所有最近一级 p 标签元素 */
```

- 元素 1 和 元素 2 中间用**大于号**隔开
- 元素 1 是父级，元素 2 是子级，**最终选择的是元素 2**
- 元素 2 必须是**亲儿子**，其孙子、重孙子之类都不归他管。

## 8.4 并集选择器（重要）

**并集选择器可以选择多组标签，同时为他们定义相同的样式**。通常用于集体声明

**并集选择器**是各选择器**通过英文逗号（,）连接而成**，任何形式的选择器都可以作为并集选择器的一部分。

语法：

> 元素 1，元素 2 { 样式声明 }

上述语法表示**选择元素 1 和 元素 2**

例如：

```css
ul,div { 样式声明 } /* 选择 ul 和 div 标签元素 */
```

- 元素 1 和 元素 2 中间用***\*逗号隔开\****
- 逗号可以理解为**和**的意思
- 并集选择器通常用于集体声明

## 8.5 伪类选择器

伪类选择器用于向某些选择器添加特殊的效果，比如给链接添加特殊效果，或选择第 n 个元素

伪类选择器书写最大的特点是用冒号（:）表示，比如 :hover、:first-child

伪类选择器很多，比如有链接伪装，结果伪装等

## 8.6 链接伪类选择器

```css
a:link        /* 选择所有未被访问的链接 */    



a:visited     /* 选择所有已被访问的链接 */



a:hover       /* 选择鼠标指针位于其上的链接 */



a:active      /* 选择活动链接（鼠标按下未弹起的链接）*/
```

**链接伪类选择器注意事项**

\1. 为了确保生效，请按照 **LVHA** 的顺序声明：link，visited，hover，active

\2. 因为 a 链接在浏览器中具有默认样式，所以我们实际工作中都需要给链接单独指定样式

**链\**接\**伪类选择器实际工作开发中的写法**

```css
/* a 是标签选择器 所有的链接*/



a{



    color: gray;



}



/* :hover 是链接伪类选择器 鼠标经过 */



a:hover {



    color: red;



    /* 鼠标经过的时候，由原来的 灰色 变成了红色 */



}
```

## 8.7 focus 伪类选择器

**:focus 伪类选择器**用于选择获得焦点的表单元素。

焦点就是光标，一般情况 <input> 类表单元素才能获取，因此这个选择器也主要针对于表单元素。

```css
input:focus {



    background-color:yellow;



}
```

## 8.8 复合选择器总结

![img](https://img-blog.csdnimg.cn/f2b29c818b1b47798e828803d71240ab.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_20,color_FFFFFF,t_70,g_se,x_16)

# ***\*9. CSS 的元素显示模式\****

## 9.1 什么是元素显示模式

作用：网页的标签非常多，在不同地方会用到不同类型的标签，了解他们后***\*可以更好的布局网页\****。

元素显示模式就是**元素（标签）以什么方式进行显示**。

HTML 元素一般分为**块元素**和**行内元素**两种类型。

## 9.2 块元素

常见的块元素有<h1>~<h6>、<p>、<div>、<ul>、<ol>、<li>等其中***\*<div>标签是最典型的块元素\****

块元素的**特点**：

- 比较霸道，自己独占一行。
- 高度，宽度，外边距以及内边距都可以控制。
- 宽度默认是容器（父级宽度）的100%。
- 是一个容器及盒子，里面可以放行内或块级元素。

**注意**：

- 文字类的元素内不能使用块级元素。
- <p>标签主要用于存放文字，因此<p>里面不能放块级元素，特别是不能放<div>。
- 同理，<h1>~<h6>等都是文字类块级标签，里面也不能放其他块级元素。

## 9.3 行内元素

常见的行内元素有<a>、<strong>、<b>、<em>、<i>、<del>、<s>、<ins>、<u>、<span>等，其中**<span>**标签是**最典型的行内元素**。有的地方也将行内元素称为**内联元素**。

行内元素的特点：

- 相邻行内元素在一行上，一行可以显示多个。
- 高、宽直接设置是无效的。
- 默认宽度就是它本身内容的宽度。
- 行内元素只能容纳文本或其他行内元素。

**注意：**

- 链接里面不能再放链接
- 特殊情况链接<a>里面可以放块级元素，但是给<a>转换一下块级模式最安全。

## 9.4 行内块元素

在行内元素中有几个特殊的标签 <img/>、<input/>、<td>，它们同时具有块元素和行内元素的特点

行内块元素的特点：

- 和相邻行内元素（行内块）在一行上，但是他们之间会有空白缝隙。一行可以显示多个。
- 默认宽度就是它本身内容的宽度（行内元素特点）。
- 高度，行高，外边距以及内边距都可以控制（块级元素特点）。

## 9.5 元素显示模式转换

特殊情况下，我们需要元素模式的转换，简单理解：一个模式的元素需要另外一种模式的特性。

比如想要增加链接<a>的触发范围。

- 转换为块元素：display:block；
- 转换为行内元素：display:inline；
- 转换为行内块：display:inline-block；

## 9.6 案例：简洁版小米侧边栏

案例的核心思路分为两步：

\1. 把链接 a 转换为块级元素，这样链接就可以单独占一行，并且有宽度和高度。

\2. 鼠标经过 a 给 链接设置背景颜色。

代码示例：

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>简洁版小米侧边栏</title>



    <style>



        /* 1. 把 a 转换为块级元素*/



        a{



            display: block;



            width: 230px;



            height: 40px;



            background-color: #55585a;



            font-size: 14px;



            color: #fff;



            text-decoration: none;



            text-indent: 2em;



        }



        a:hover{



            background-color: #ff6700;



            



        }



    </style>



</head>



<body>



    <a href="#">手机 电话卡</a>



    <a href="#">电视 盒子</a>



    <a href="#">笔记本 平板</a>



    <a href="#">出行 穿戴</a>



    <a href="#">智能 路由器</a>



    <a href="#">健康 儿童</a>



    <a href="#">耳机 音响</a>



</body>



</html>
```

CSS 没有给我们提供文字垂直居中的代码。我们可以通过一个小技巧来实现。

让文字的行高等于盒子的高度，就可以让文字在当前盒子内垂直居中。

# ***\*10. CSS 的背景\****

通过 CSS 背景属性，可以给页面元素添加背景样式。

背景属性可以设置背景颜色、背景图片、背景平铺、背景图片位置、背景图像固定等。

## 10.1 背景颜色

background-color 属性定义了元素的背景颜色。

```css
background-color:颜色值;
```

一般情况下元素背景颜色默认值是 transparent（透明），我们也可以手动指定背景颜色为透明色

## 10.2 背景图片

***\*background-image\**** 属性描述了元素的背景图像。实际开发常见于 logo 或者一些装饰性的小图片或者是超大的背景图片，优点是非常便于控制位置。

```css
background-image : none | url (url)
```

| **参数值** | **作用**                       |
| ---------- | ------------------------------ |
| none       | 无背景图（默认的）             |
| url        | 使用绝对或相对地址指定背景图像 |

## 10.3 背景平铺

如果需要在 HTML 页面上对背景图像进行平铺，可以使用 ***\*background-repeat\**** 属性

```css
background-repeat: repeat | no-repeat | repeat-x |repeat-y
```

| **参数值** | **作用**                             |
| ---------- | ------------------------------------ |
| repeat     | 背景图像在纵向和横向上平铺（默认的） |
| no-repeat  | 背景图像不平铺                       |
| repeat-x   | 背景图像在横向上平铺                 |
| repeat-y   | 背景图像在纵向上平铺                 |

## 10.4 背景图片位置

利用 ***\*background-position\**** 属性可以改变图片在背景中的位置

```css
background-position: x y;
```

参数代表的意思是：x坐标和y坐标，可以使用***\*方位名词\****或者***\*精确单位\****

| 参数值   | 说明                                                        |
| -------- | ----------------------------------------------------------- |
| length   | 百分数\|由浮点数字和单位识别符组成的长度                    |
| position | top \| center \| bottom \| left \| center \| right 方位名词 |

\1. 参数是方位名词

- 如果指定的两个值都是方位名词，则两个值前后顺序无关，比如 left top 和 top left 效果一致
- 如果只指定了一个方位名词，另一个值省略，则第二个值默认居中对齐

\2. 参数是精确单位

- 如果参数值是精度坐标，那么第一个肯定是x坐标，第二个一定是y坐标
- 如果只指定一个数值，那该数值一定是x坐标，另一个默认垂直居中

\3. 参数是混合单位

- 如果指定的两个值是精确单位和方位名词混合使用，则第一个值是x坐标，第二个值是y坐标

## 10.5 背景图像固定（背景附着）

background-attachment 属性设置背景图像是否固定或者随着页面的其余部分滚动

background-attachment 后期可以制作视差滚动的效果

```css
background-attachment : scroll | fixed
```

| 参数   | 作用                     |
| ------ | ------------------------ |
| scroll | 背景图像是随对象内容滚动 |
| fixed  | 背景图像固定             |

## 10.6 背景复合写法

为了简化背景属性的代码，我们可以将这些属性合并简写在同一个属性 ***\*background\**** 中，从而节约代码量

当使用简写属性时，没有特定的书写顺序，一般习惯约定顺序为：

background：背景颜色 背景图片地址 背景平铺 背景图像滚动 背景图片位置；

```css
background: transparent url(image.jpg) repeat-y fixed top;
```

这是实际开发中，我们更提倡的写法

## 10.7 背景色半透明

CSS3 为我们提供了背景颜色半透明的效果

```css
background rgba（0，0，0， 0.3）;
```

- 最后一个参数是 alpha 透明度，取值范围在 0~1 之间
- 我们习惯把 0.3 的 0 省略掉，写为 background rgba(0,0,0,3);
- 注意：背景半透明是指盒子背景半透明，盒子里面的内容不受影响
- CSS3 新增属性，是 IE9 + 版本浏览器才支持的
- 但是现在实际开发，我们不太关注兼容性写法了，可以放心使用
- 最后一个参数是 alpha 透明度，取值范围在 0~1 之间

## 案例：五彩导航

练习价值：

1. 链接属于行内元素，但是此时需要宽度高度，因此需要模式转换
2. 里面文字需要水平居中和垂直居中，因此需要单行文字垂直居中的代码
3. 链接里面需要设置背景图片，因此需要用到背景的相关属性设置
4. 鼠标经过变化背景图片，因此需要用到链接伪类选择器

### 制作效果

![img](https://img-blog.csdnimg.cn/46a3812f249743bbb18082c27d13be9e.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_20,color_FFFFFF,t_70,g_se,x_16)

鼠标移动到标题栏时 ![img](https://img-blog.csdnimg.cn/3c773afb974444cf82505181a3d862de.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_20,color_FFFFFF,t_70,g_se,x_16)



### 代码示例

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>五彩导航</title>



    <style>



        .nav a{



            display: inline-block;



            width: 120px;



            height: 50px;



            background-color: pink;



            text-align: center;



            line-height: 50px;



            color: #fff;



            text-decoration: none;



        }



        .nav .bg1{



            background: url(202126203057-lab04/images/01.jpg);



        }



        .nav .bg1:hover{



            background: url(202126203057-lab04/images/06.jpg);



        }



        .nav .bg2{



            background: url(202126203057-lab04/images/02.jpg);



        }



        .nav .bg2:hover{



            background: url(202126203057-lab04/images/1.jpg);



        }



        .nav .bg3{



            background: url(202126203057-lab04/images/03.jpg);



        }



        .nav .bg3:hover{



            background: url(202126203057-lab04/images/1.png);



        }



        .nav .bg4{



            background: url(202126203057-lab04/images/04.jpg);



        }



        .nav .bg4:hover{



            background: url(202126203057-lab04/images/11.png);



        }



        .nav .bg5{



            background: url(202126203057-lab04/images/05.jpg);



        }



        .nav .bg5:hover{



            background: url(202126203057-lab04/images/2.jpg);



        }



    </style>



</head>



<body>



    <div class="nav">



        <a href="#" class="bg1">五彩导航</a>



        <a href="#" class="bg2">五彩导航</a>



        <a href="#" class="bg3">五彩导航</a>



        <a href="#" class="bg4">五彩导航</a>



        <a href="#" class="bg5">五彩导航</a>



    </div>



</body>



</html>
```

# ***\*11. CSS 的三大特性\****

css 有三个非常重要的三个特性：层叠性，继承性，优先级。

## 11.1 层叠性

相同选择器给设置相同的样式，此时一个样式就会***\*覆盖（层叠）\****另一个冲突的样式。层叠性主要解决样式冲突的问题

层叠性原则：

- 样式冲突，遵循的原则是***\*就近原则\****，哪个样式离结构近，就执行哪个样式
- 样式不冲突，不会层叠

## 11.2 继承性

现实中的继承：我们继承了父亲的姓

CSS中的继承：子标签会继承父标签的某些样式，如文本颜色和字号，简单的理解就是：子承父业

- 恰当地使用继承可以简化代码，降低 CSS 样式的复杂性
- 子元素可以继承父元素的样式（text-，font-，line- 这些元素开头的可以继承，以及color）

**行高的继承性**

```css
body {



    font:12px/1,5 Microsoft YaHei;



}
```

- 行高可以跟单位也可以不跟单位
- 如果子元素没有设置行高，则会继承父元素的行高为1.5
- 此时子元素的行高是：当前子元素的文字大小*1.5
- ***\*body行高 1.5 这样写法最大的优势就是里面子元素可以根据自己文字大小自动调整行高\****

## 11.3 优先级

当同一个元素指定多个选择器，就会有优先级的产生

- 选择器相同，则执行层叠性
- 选择器不同，则根据选择器权重执行

![img](https://img-blog.csdnimg.cn/3c198a3dbddc437c93ae8c3c93b94adc.png?x-oss-process=image/watermark,type_d3F5LXplbmhlaQ,shadow_50,text_Q1NETiBA5bOw5LiN5LqM5a2Q55qE5bCP6L-35byf,size_12,color_FFFFFF,t_70,g_se,x_16)

 优先级注意点：

1. 权重是有4组数字组成，但是不会有进位
2. 可以理解为类选择器永远大于元素选择器，id选择器永远大于类选择器以此类推
3. 等级判断从左向右，如果某一位数值相同，则判断下一位数值
4. ***\*继承的权重是0\****，如果该元素没有直接选中，不管父亲权重多高，子元素得到的权重都是0

**权重叠加：**如果是复合选择器，则会有权重叠加，需要计算权重

- div ul li -> 0,0,0,3
- .nav ul li -> 0,0,1,2
- a:hover -> 0,0,1,1
- .nav a -> 0,0,1,1

# ***\*12. 盒子模型\****

页面布局要学习三大核心，盒子模型，浮动和定位，学习好盒子模型能非常好的帮助我们布局页面

## 12.1 看透网页布局的本质

网页布局过程：

\1. 先准备好相关的网页元素，网页元素基本都是盒子 Box

\2. 利用 CSS 设置好盒子样式，然后摆放到相应位置

\3. 忘盒子里面装内容

网页布局的核心本质：就是利用 CSS 摆盒子

## 12.2 盒子模型 （Box Model）组成

所谓 **盒子模型：**就是把 HTML 页面中的布局元素看作是一个矩形的盒子，也就是一个装内容的容器。CSS 盒子模型本质上是一个盒子，封装周围的 HTML 元素，它包括：边框，外边距，内边距和实际内容



![img](https://img-blog.csdnimg.cn/50b9f8ed3b1d4e79b6197c2660313d79.png)

##  12.3 边框（border）

border 可以设置元素的边框，边框有三部分组成边框宽度（粗细）边框样式 边框颜色

语法：

```css
border: border-width || border-style || border-color
```

| **属性**     | **作用**               |
| ------------ | ---------------------- |
| border-width | 定义边框粗细，单位是px |
| border-style | 边框的样式             |
| border-color | 边框颜色               |

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>Document</title>



    <style>



        div {



            width: 300px;



            height: 200px;



            /* border-width 边框的粗细 一般情况下都用 px */



            border-width: 5px;



            /* border-style 边框的样式 solid 实线边框 dashed 虚线边框 dotted 点线边框 */



            border-style: solid;



            border-color: pink;



        }



    </style>



</head>



<body>



    



</body>



</html>
```

CSS 边框属性允许你指定一个元素边框的**样式和颜色**

边框简写：

```css
border: 1px solid red; 没有顺序
```

边框分开写法：

```css
border-top: 1px solid red; /* 只设定上边框，其余同理 */
```

课堂检测：给定一个 200*200 的盒子，设置上边框为红色，其余边框为蓝色（注意层叠性）

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>Document</title>



    <style>



        div {



            width: 200px;



            height: 200px;



            border: 1px solid blue;



            border-top: 1px solid red;



        }



    </style>



</head>



<body>



    <div></div>



</body>



</html>
```

## 12.4 表格的细线边框

**border-collapse** 属性控制浏览器绘制表格边框的方式。它控制相邻单元格的边框。

语法：

```css
border-collapse:collapse;
```

- collapse 单词是合并的意思
- border-collapse:collapse; 表示相邻边框合并在一起

## 12.5 边框会影响盒子实际大小

边框会额外增加盒子的实际大小。因此我们有两种方案解决。

\1. 测量盒子大小的时候，不量边框。

\2. 如果测量的时候包含了边框，则需要 width/height 减去边框宽度。

## 12.6 内边距（padding）

**padding** 属性用于设置内边距，即边距与内容之间的距离。

| **属性**       | **作用** |
| -------------- | -------- |
| padding-left   | 左内边距 |
| padding-right  | 右内边距 |
| padding-top    | 上内边距 |
| padding-bottom | 下内边距 |

padding 属性（简写属性）可以有一到四个值。

| **值的个数**                | **表达意思**                                         |
| --------------------------- | ---------------------------------------------------- |
| padding:5px;                | 1个值，代表上下左右都有5像素内边距                   |
| padding:5px 10px;           | 2个值，代表上下内边距是5像素，左右内边距是10像素     |
| padding:5px 10px 20px;      | 3个值，代表上内边距5像素                             |
| padding:5px 10px 20px 30px; | 4个值，上是5像素 右10像素，下20像素，左30像素 顺时针 |

当我们给盒子指定 padding 值之后，发生了 2 件事情:

\1. 内容和边距有了距离，添加了内边距。

\2. padding 影响了盒子实际大小。

也就是说，如果盒子已经有了宽度和高度，此时再指定内边框，会撑大盒子。

**解决方案：**

如果保证盒子跟效果图大小保持一致，则让 **width/height 减去多出来的内边距大小**即可。

**案例：新浪导航案例-padding影响盒子好处**

padding内边距可以撑开盒子，我们可以做非常巧妙的运用。

因为每个导航栏里面的字数不一样多，我们可以不用给每个盒子宽度了，直接给padding最合适

相关取值：

\1. 上边框为3像素，颜色为 #ff8500

\2. 下边框为1像素，颜色为 #edeef0

\3. 盒子高度为41像素，背景颜色为 #fcfcfc

\4. 文字颜色为 #4c4c4c

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>新浪导航</title>



    <style>



        .nav{



            height: 41px;



            border-top: 3px solid #ff8500;



            border-bottom: 1px solid #edeef0;



            background-color: #fcfcfc;



            line-height: 41px;



        }



        .nav a{



            /* a属于行内元素 此时必须要转换 行内块元素 */



            display: inline-block;



            height: 41px;



            padding: 0 20px;



            font-size: 12px;



            color: #4c4c4c;



            text-decoration: none;



        }



        .nav a:hover{



            background-color: #eee;



            color: #ff8500;



        }



    </style>



</head>



<body>



    <div class="nav">



        <a href="#">新浪导航</a>



        <a href="#">手机新浪网</a>



        <a href="#">移动客户端</a>



        <a href="#">微博</a>



        <a href="#">三个字</a>



    </div>



</body>



</html>
```

**案例：小米导航案例修改-padding影响盒子大小计算**

padding内边距可以撑开盒子，有时候，也会让我们去修改宽度

所以小米侧边栏这个案例，文字距离左侧的距离不应该用 text-indent 这样不准确

实际开发的做法是给 padding 值，这样更加准确

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>小米导航</title>



    <style>



        a {



            display: block;



            width: 230px;



            height: 40px;



            background-color: #55585a;



            font-size: 14px;



            color: #fff;



            text-decoration: none;



            text-indent: 2em;



            padding-left: 30px;



            line-height: 40px;



        }



        a:hover {



            background-color: #ff6700;



        }



    </style>



</head>



<body>



    <div>



        <a href="#">手机 电话卡</a>



        <a href="#">电报 盒子</a>



        <a href="#">笔记本 平板</a>



        <a href="#">出行 穿戴</a>



        <a href="#">智能 路由器</a>



        <a href="#">健康 儿童</a>



        <a href="#">耳机 音响</a>



    </div>



</body>



</html>
```

如果盒子本身没有指定 width/height 属性，则此时 padding 不会撑开盒子大小。

## 12.7 外边距（margin）

**margin** 属性用于设置外边距，即控制盒子和盒子之间的距离。

| **属性**      | **作用** |
| ------------- | -------- |
| margin-left   | 左外边距 |
| margin-righ   | 右外边距 |
| margin-top    | 上外边距 |
| margin-bottom | 下外边距 |

margin 简写方式代表的意义跟 paddin 完全一致

外边距可以让块级盒子**水平居中**，但是必须满足两个条件：

\1. 盒子必须指定了宽度（width）

\2. 盒子左右的外边距都设置为 auto

```css
.header { width: 960px; margin: 0 auto;}
```

常见的写法，以下三种都可以：

- margin-left: auto ; margin-right: auto ;
- margin: auto;
- margin: 0 auto;

**注意：**以上方法是让块级元素水平居中，行内元素或者行内块元素水平居中给其父元素添加      text-align: center; 即可。

## 12.8 外边距合并

使用 **margin** 定义块元素的**垂直外边距**时，可能会出现外边距的合并。

### 1.相邻块元素垂直外边距的合并

当上下相邻的两个块元素（兄弟关系）相遇时，如果上面的元素有下外边距 margin-bottom，下面的元素有上外边距 margin-top，则他们之间的垂直间距不是 margin-bottom 与 margin-top 之间。**取两个值中的较大者这种现象被称为相邻块元素垂直外边距的合并。**

**解决方案：**

**尽量只给一个盒子添加 margin 值。**

### 2. 嵌套块元素垂直外边距的塌陷

对于两个嵌套关系（父子关系）的块元素，父元素有上外边距同时子元素也有上外边距，此时父元素会塌陷较大的外边距值。

**解决方案：**

\1. 可以为父元素定义上边距

\2. 可以为父元素定义上内边距

\3. 可以为父元素添加 overflow:hidden

还有其他方法，比如浮动、固定，绝对定位的盒子不会有塌陷问题，后面咱们再总结

## 12.9 清楚内外边距

网页元素很多都带有默认的内外边距，而且不同浏览器默认的也不一致。因此我们在布局前，首先要清楚下网页元素的内外边距

```css
* {



    padding:0;    /* 清楚内边距 */



    margin:0;    /* 清楚外边距 */



}
```

注意：行内元素为了照顾兼容性，尽量只设置左右内外边距，不要设置上下内外边距。但是转换为块级和行内块元素就可以了

PS 基本操作

- **文件→打开**：可以打开我们要测量的图片
- **Ctrl + R**：可以打开标尺，或者 **视图→标尺**
- 右击标尺，把里面的单位改为像素
- **Ctrl + 加号（+）**可以放大视图，**Ctrl + 减号（-）**可以缩小视图
- **按住空格键**，鼠标可以变成小手，拖动 PS 视图
- 用**选区**拖动，可以测量大小
- **Ctrl + D** 可以取消选取，或者在**旁边空白处点击一下**也可以取消选区

![img](https://img-blog.csdnimg.cn/5802ba99c9184dba8459d7bec722c56c.png)

 **综合案例 - 产品模块**

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>综合案例-产品模块</title>



    <style>



        * {



            margin: 0;



            padding: 0;



        }



        body {



            background-color: #f5f5f5;



        }



        a {



            color: #333;



            text-decoration: none;



        }



        .box {



            width: 298px;



            height: 415px;



            background-color: #fff;



            /* 让块级的盒子水平居中对齐 */



            margin: 100px auto;



        }



        .box img{



            /* 图片的宽度和父亲一样宽 */



            width: 100%;



        }



        .review {



            height: 70px;



            font-size: 14px;



            /* 因为这个段落没有 width 属性 所有 padding 不会撑开盒子的宽度 */



            padding: 0 28px;



            margin-top: 30px;



        }



        .appraise {



            font-size: 12px;



            color: #b0b0b0;



            margin-top: 20px;



            padding: 0 28px;



        }



        .info {



            font-size: 14px;



            margin-top: 15px;



            padding:  0 28px;



        }



        .info h4 {



            display: inline-block;



            font-weight: 400;



        }



        .info span {



            color: #ff6700;



        }



        .info em{



            font-style: normal;



            color: #ebe4e0;



            margin: 0 6px 0 15px;



        }



    </style>



</head>



<body>



    <div class="box">



        <img src="airpods.png" alt="">



        <p class="review">快递牛，整体不错蓝牙可以说秒连。红米给力</p>



        <div class="appraise">来自于 117384232 的评价</div>



        <div class="info">



            <h4> <a href="#">Redmi AirDots真无线蓝...</a></h4>



            <em>|</em>



            <span> 99.9元</span>



        </div>



    </div>



</body>



</html>
```

 **效果图**

![img](https://img-blog.csdnimg.cn/d47023554b3e4ba2ae0e7a8e0f7cb62e.png)

 **Pink 老师总结**

**1. 布局为啥用不同盒子，我只想用div？**

标签都是有语义的，合理的地方用合理的标签。比如产品标题 就用 j，大量文字段落就用 p

**2. 为啥用那么多类名？**

类名就是给每个盒子起了一个名字，可以更好的找到这个盒子，选取盒子更容易，后期维护也方便

**3. 到底用 margin 还是 padding？**

大部分情况两个可以混用，两者各有优缺点，但是根据实际情况，总是有更简单的方法实现

**4. 自己做没有思路？**

布局有很多种实现方式，大家可以开始先模仿Pink 老师的写法，然后再做出自己的风格

最后大家一定多运用辅助工具，比如屏幕画笔，ps等等

**综合案例 - 快报模板**

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>快报模板</title>



    <style>



        * {



            margin: 0;



            padding: 0;



        }



        li {



            /* 去掉 i 前面的小圆点 */



            list-style: none;



        }



        .box {



            width: 248px;



            height: 163px;



            border: 1px solid #ccc;



            margin: 100px auto;



        }



        .box h3{



            height: 32px;



            border-bottom: 1px dotted #ccc;



            font-size: 14px;



            font-weight: 400;



            line-height: 32px;



            padding-left: 15px;



        }



        .box ul li a:hover{



            text-decoration: underline;



        }



        .box ul li a {



            font-size: 12px;



            color: #666;



            text-decoration: none;



        }



        .box ul li {



            height:  23px;



            line-height: 23px;



            padding-left: 20px;



        }



        .box ul{



            margin-top: 7px;



        }



    </style>



</head>



<body>



    <div class="box">



        <h3>品优购快报</h3>



        <ul>



            <li><a href="#">【特惠】 爆款耳机5折秒！</a></li>



            <li><a href="#">【特惠】 母亲节，健康好礼低至5折</a></li>



            <li><a href="#">【特惠】 switch游戏机半价秒！</a></li>



            <li><a href="#">【特惠】 9.9元洗100张照片！</a></li>



            <li><a href="#">【特惠】 长虹只能空调立省1000！</a></li>



        </ul>



    </div>



</body>



</html>
```

# ***\*13. 圆角边框（重点）\****

在 CSS3 中，新增了**圆角边框**样式，这样我们的盒子就可以变圆角了。

**border-radius** 属性用于设置元素的外边框圆角。

语法：

```css
border-radius:length;
```

**radius 半径（圆的半径）原理：**（椭）圆与边框的交集形成圆角效果

- 参数值可以为**数值**或**百分比**的形式
- 如果是**正方形**，想要设置为一个圆，把数值修改为**高度或者宽度的一半**即可，或者直接写**50%**
- **如果是个矩形，设置为高度的一半就可以做**
- 该属性是一个**简写属性**，可以跟四个值，分别代表**左上角、右上角、右下角、左下角**
- 分开写：**border-top-left-radius、border-top-right-radius、border-bottom-right-radius 和 border-bottom-left-radius**

# ***\*14. 盒子阴影（重点）\****

CSS3 中新增了盒子阴影，我们可以使用 box-shadow 属性为盒子添加阴影

语法：

```css
box-shadow: h-shadow v-shadow blur spread color inset;
```

| **值**   | **描述**                               |
| -------- | -------------------------------------- |
| h-shadow | 必需，水平阴影的位置，允许负值         |
| v-shadow | 必需，垂直阴影的位置，允许负值         |
| blur     | 可选，模糊距离                         |
| spread   | 可选，阴影的尺寸                       |
| color    | 可选，阴影的颜色，请参阅 CSS 颜色值    |
| inset    | 可选，将外部阴影（outset）改为内部阴影 |

***\*注意：\****

***\*1. 默认的是外阴影（outset），但是不可以写这个单词，否则导致阴影无效\****

***\*2.盒子阴影不占用空间，不会影响其他盒子排列\****

# ***\*15. 文字阴影\****

在 CSS3 中，我们可以使用 **text-shadow** 属性将阴影应用于文本。

语法：

```css
text-shadow: h-shadow v-shadow blur color;
```

| 值       | 描述                              |
| -------- | --------------------------------- |
| h-shadow | 必需，水平阴影的位置，允许负值    |
| v-shadow | 必需，垂直阴影的位置，允许负值    |
| blur     | 可选，模糊的距离                  |
| color    | 可选，阴影的颜色。参阅 CSS 颜色值 |

# ***\*16. 浮动（float）\****

## 16.1 传统网页布局的三种方式

网页布局的本质——用 CSS 来摆放盒子，把盒子摆放到相应的位置。

CSS 提供了三种传统布局方式简单说，就是盒子如何进行行排列顺序：

- 普通流（标准流）
- 浮动
- 定位

## 16.2 标准流（普通流/文档流）

所谓的标准流：就是标签按照规定好默认方式排序

\1. 块级元素会独占一行，从上向下顺序排列。

常用元素：div、hr、p、h1-h6、ul、ol、dl、form、table

\2. 行内元素会按照顺序，从左到右顺序排列，碰到父元素边缘则自动换行。

常用元素：span、a、i、em 等

以上都是标准布局，我们前面学习的就是标准流，标准流就是最基本的布局方式。

这三种布局方式都是用来摆放盒子的，盒子摆放到合适位置，布局自然就完成了。

注意：实际开发中，一个页面基本都包含了这三种布局方式，（后面移动端学习新的布局方式）。

## 16.3 为什么需要浮动？

\1. 如何让多个块级盒子（div）水平排列成一行？

比较难，虽然转换为行内块元素可以实现一行显示，但是他们之间会有大的空白缝隙，很难控制。

总结：有很多的布局效果，标准流没有办法完成，此时就可以利用浮动完成布局，因为浮动可以改变元素标签默认的排列方式。

浮动最典型的应用：可以让多个块级元素一行内排列显示。

网页布局第一标准：多个块级元素纵向排列找标准流，多个块级元素横向排列栈浮动。

## 16.4 什么是浮动？

float 属性用于创建浮动框，将其移动到一边，直到左或右边缘触及包含块或另一个浮动框的边缘

语法：

```css
选择器{ float: 属性值; }
```

| **属性值** | **描述**             |
| ---------- | -------------------- |
| none       | 元素不浮动（默认值） |
| left       | 元素向左浮动         |
| right      | 元素向右浮动         |

## 16.5 浮动特性（重难点）

加了浮动之后的元素，会具有很多特性，需要我们掌握的。

1. 浮动元素会脱离标准流（脱标）
2. 浮动的元素会一行内显示并且元素顶部对齐
3. 浮动的元素会具有行内块元素的特性。

设置了浮动（float）的元素最重要特性：

1. 脱离标准普通流的控制（浮）移动到指定位置（动），（俗称脱标）
2. 浮动的盒子不再保留原先的位置

\2. 如果多个盒子都设置了浮动，则它们会按照属性值一行内显示并且顶端对齐排列。

**注意：**浮动的元素是互相贴靠在一起的（不会有缝隙），如果父级宽度装不下这些浮动的盒子，多出的盒子会另起一行对齐。

\3. 浮动元素会具有行内块元素特性。

任何元素都可以浮动，不管原先是什么模式的元素，添加浮动之后具有行内块元素相似的特性。

- 如果块级盒子没有设置宽度，默认宽度和父级一样宽，但是添加浮动后，它的大小根据内容来决定
- 浮动的盒子中间是没有缝隙的，是紧挨着一起的
- 行内元素同理

## 16.6 浮动元素经常和标准父级搭配使用

为了约束浮动元素位置，我们网页布局一般采取的策略是：

先用标准流的父元素排列上下位置，在内部子元素采取浮动排列左右位置，符合网页布局第一准则

![img](https://img-blog.csdnimg.cn/bc2cbc3202ed423caf1679df6d2a372c.png)

 案例：小米布局案例

网页布局第二准则：先设置盒子大小，之后设置盒子的位置

![img](https://img-blog.csdnimg.cn/222e7681e7f3422692e6e2cc8036cfc8.png)

 代码示例

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>浮动元素搭配标准流父盒子1</title>



    <style>



        .box {



            width: 1200px;



            height: 460px;



            background-color: pink;



            margin: 0 auto;



        }



        .left {



            float: left;



            width: 230px;



            height: 460px;



            background-color: purple;



        }



        .right {



            float: left;



            width: 970px;



            height: 460px;



            background-color: skyblue;



        }



    </style>



</head>



<body>



    <div class="box">



        <div class="left">左侧</div>



        <div class="right">右侧</div>



    </div>



</body>



</html>
```

------

![img](https://img-blog.csdnimg.cn/eb6d4a63780c494db11b0165aa6db9a2.png)

 代码示例

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>浮动元素搭配标准流父盒子2</title>



    <style>



        * {



            margin: 0;



            padding: 0;



        }



        li {



            list-style: none;



        }



        .box {



            width: 1226px;



            height: 285px;



            background-color: pink;



            margin: 0 auto;



        }



        .box li {



            width: 296px;



            height: 285px;



            background-color: purple;



            float: left;



            margin-right: 14px;



        }



        .box .last {



            margin-right: 0;



        }



    </style>



</head>



<body>



    <ul class="box">



        <li>1</li>



        <li>2</li>



        <li>3</li>



        <li class="last">4</li>



    </ul>



</body>



</html>
```

------

![img](https://img-blog.csdnimg.cn/fd9f81e102544e288a5a9eb629baf623.png)

 代码示例

```html
<!DOCTYPE html>



<html lang="en">



<head>



    <meta charset="UTF-8">



    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <meta name="viewport" content="width=device-width, initial-scale=1.0">



    <title>浮动布局练习3</title>



    <style>



        .box {



            width: 1226px;



            height: 615px;



            background-color: pink;



            margin: 0 auto;



        }



        .left {



            float: left;



            width: 234px;



            height: 615px;



            background-color: purple;



        }



        .right {



            float: left;



            width: 992px;



            height: 615px;



            background-color: skyblue;



        }



        .right>div {



            float: left;



            width: 234px;



            height: 300px;



            background-color: gray;



            margin-left: 14px;



            margin-bottom: 14px;



        }



    </style>



</head>



<body>



    <div class="box">



        <div class="left">左青龙</div>



        <div class="right">



            <div>1</div>



            <div>2</div>



            <div>3</div>



            <div>4</div>



            <div>5</div>



            <div>6</div>



            <div>7</div>



            <div>8</div>



        </div>



    </div>



</body>



</html>
```

# ***\*17 常见网页布局\****

## 17.1 常见网页布局

![img](https://img-blog.csdnimg.cn/73ded34be0a942e1b7cf6bfb9d189bc6.png)

------

 ![img](https://img-blog.csdnimg.cn/7d985245b64740428ee3adca7ed36b38.png)

##  17.2 浮动布局注意点

\1. 浮动和标准流的父盒子搭配

***\*先用标准流的父元素排列上下位置，之后内部子元素采取浮动排列左右位置\****

\2. 一个元素浮动了，理论上其余的兄弟元素也要浮动

一个盒子里面有多个子盒子，如果其中一个盒子浮动了，那么其他兄弟也应该浮动，以防出现问题

***\*浮动的盒子只会影响浮动盒子后面的标准流，不会影响前面的标准流\****

# ***\*18. 清除浮动\****

## 18.1 为什么需要清除浮动？

由于父级盒子很多情况下，不方便给高度，但是子盒子浮动又不占有位置，最后父级盒子高度为0时，就会影响下面的标准流盒子。

- 由于浮动元素不再占用原文档流的位置，所以它会对后面的元素排版产生影

## 18.2 清除浮动本质

- 清除浮动的本质是清除浮动元素造成的影响
- 如果父盒子本身有高度，则不需要清除浮动
- **清除浮动之后，父级就会根据浮动的子盒子自动检测高度，父级有了高度，就不会影响下面的标准流了**

## 18.3 清除浮动

语法：

```css
选择器{clear:属性值;}
```

| 属性值 | 描述                                       |
| ------ | ------------------------------------------ |
| left   | 不允许左侧有浮动元素（清除左侧浮动的影响） |
| right  | 不允许右侧有浮动元素（清除右侧浮动的影响） |
| both   | 同时清除左右两侧浮动的影响                 |

我们实际工作中，几乎只用clear:both;

清除浮动的策略是：闭合浮动

\1. **额外标签法**也称为隔墙法，是 W3C 推荐的做法。

**2. 父级添加 overflow 属性**

**3. 父级添加 after 属性**

**4. 父级添加双伪元素**

## 额外标签法

**额外标签法**也称为隔墙法，是 W3C 推荐的做法。

**额外标签法**会在浮动元素末尾添加一个空的标签。例如<div style="clear:both;"></div>,或其他标签

- 优点：通俗易懂，书写方便
- 缺点：添加许多无意义的标签，结构化较差

注意：要求这个新的空标签必须是块级元素

总结：

\1. 清除浮动本质是？

**清除浮动的本质是清除浮动元素脱离标准流造成的影响**

\2. 清除浮动策略是？

**闭合浮动，只让浮动在父盒子内部影响，不影响父盒子外面的其他盒子**

\3. 额外标签法

隔墙法，就是在最后一个浮动的子元素后面添加一个额外标签，添加清除浮动样式

实际工作可能会遇到，但是不常用

父级添加 overflow

可以给父级添加 **overflow** 属性，将其属性设置为 hidden，auto或scroll

子不教，父之过，注意是给父元素添加代码

- 优点：代码简洁
- 缺点：无法显示溢出的部分