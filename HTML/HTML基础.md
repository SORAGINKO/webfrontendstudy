[toc]

## 前言

&emsp;&emsp;HTML可以说是一个相当简单的语言，从完全不懂到入门能写出一个简单的纯HTML网页，带有标题，表格，列表，引用等等只需要半天时间。网页的骨架写好了，再学CSS去美化就变得相当轻松。

## Preparation

HTML是由一个一个的标签组成的元素组成的，标签是像这样`<tag>`的东西。标签分两种，有开始和结束两个标签的和自闭合标签（或者叫空元素）。前者主要在body里如p div span ，后者主要在head里如meta, link ,base。
在HTML中空元素不用写闭合的斜杠，在XHTML中必须要写，比如</br>

<tagname key="value" key2="value2">text or tag</tagename>
大部分标签都是这样，key=“value"部分是属性。

注释 <!-- 这是一个注释 --> ，HTML中的注释比较难写

### 1常用文本格式化标签

**标题 **h1-h6标题，h1最大，h6最小。放大big，缩小small，
**段落** p标签创建一个段落，缩进不能用空格，请用字符实体也叫转义字符：`&emsp;&emsp;`

**文本格式化**：
粗体`<b> 粗体 </b>`; 斜体 `<i> 斜体 </i>`

注意：通常用标签 `<strong>` 替换加粗标签 `<b> `来使用, `<em>` 替换 `<i>`标签使用。

**代码** `<code>代码<code>`，
<kbd>键盘输入</kbd>
<tt>打字机文本</tt>
<samp>计算机代码样本</samp>
<var>计算机变量</var>

这些标签常用于显示计算机/编程代码。

**预格式文本**`<pre> </pre>`
**上标下标**`<sub>下标<sub> <sup>上标</sup>`
**缩写**（两种方式abbr和acronym）
`<abbr title="fullname">shortname</abbr>
<acronym title="fullname">shortname</acronym>`
**引用**：`<q> quote</q>`
**删除线**del，**插入字**ins

### 2链接
HTML使用标签 `<a>`来设置超文本链接。
超链接可以是一个字，一个词，或者一组词，也可以是一幅图像，您可以点击这些内容来跳转到新的文档或者**当前文档中的某个部分**。
在标签`<a> `中使用了href属性来描述链接的地址。

**默认情况下，链接将以以下形式出现在浏览器中：
一个未访问过的链接显示为蓝色字体并带有下划线。
访问过的链接显示为紫色并带有下划线。
点击链接时，链接显示为红色并带有下划线。**

**常用属性**：
target="_blank" 在新页面中打开（是`_blank`不是blank，如果是blank就是在id为blank的ifram框架里打开）
id 创建一个唯一id，用于href定位或css

### 3head头部

**title 元素**:

- 定义了浏览器工具栏的标题
- 当网页添加到收藏夹时，显示在收藏夹中的标题
- 显示在搜索引擎结果页面的标题

**base元素**
base 标签描述了基本的链接地址/链接目标，该标签作为HTML文档中所有的链接标签的默认链接:

**link 标签**定义了文档与外部资源之间的关系。
link 标签通常用于链接到外部样式表:

**meta标签**描述了一些基本的元数据。
meta 标签提供了元数据.元数据也不显示在页面上，但会被浏览器解析。
META 元素通常用于指定网页的描述，关键词，文件的最后修改时间，作者，和其他元数据。

元数据可以使用于浏览器（如何显示内容或重新加载页面），搜索引擎（关键词），或其他Web服务。
meta一般放置于 <head> 区域
使用实例

```为搜索引擎定义关键词:
<meta name="keywords" content="HTML, CSS, XML, XHTML, JavaScript">
为网页定义描述内容:
<meta name="description" content="免费 Web & 编程 教程">
定义网页作者:
<meta name="author" content="Runoob">
每30秒钟刷新当前页面:
<meta http-equiv="refresh" content="30">
```

### 4图像

在 HTML 中，图像由`<img>` 标签定义。
`<img> `是空标签，意思是说，它只包含属性，并且没有闭合标签。
要在页面上显示图像，你需要使用源属性（src）。src 指 "source"。源属性的值是图像的 URL 地址。
**定义图像的语法是：**
`<img src="url" alt="some_text">`

URL 指存储图像的位置。如果名为 "pulpit.jpg" 的图像位于 www.runoob.com 的 images 目录中，那么其 URL 为 [http://www.runoob.com/images/pulpit.jpg](https://www.runoob.com/images/pulpit.jpg)。

浏览器将图像显示在文档中图像标签出现的地方。**如果你将图像标签置于两个段落之间，那么浏览器会首先显示第一个段落，然后显示图片，最后显示第二段。**也就是说img是块元素。

**常用属性**：
alt 属性用来为图像定义一串预备的可替换的文本。
height（高度） 与 width（宽度）属性用于设置图像的高度与宽度。

图像可以放在a元素中做成链接，也可以在用map元素的area属性做成区域可点击的图像地图。

### 5表格

```
<table border="1">
    <tr>
        <th>Header 1</th>
        <th>Header 2</th>
    </tr>
    <tr>
        <td>row 1, cell 1</td>
        <td>row 1, cell 2</td>
    </tr>
    <tr>
        <td>row 2, cell 1</td>
        <td>row 2, cell 2</td>
    </tr>
</table>
```

显示如下

<table border="1">
    <tr>
        <th>Header 1</th>
        <th>Header 2</th>
    </tr>
    <tr>
        <td>row 1, cell 1</td>
        <td>row 1, cell 2</td>
    </tr>
    <tr>
        <td>row 2, cell 1</td>
        <td>row 2, cell 2</td>
    </tr>
</table>
**常用属性**：
border=“__px" 边框粗度
cellspacing="number" 单元格间距，设成0让边框变成一条线
cellpadding="number" 相当于盒模型的padding

### 6列表

无序列表ul
<ul>
<li>Coffee</li>
<li>Milk</li>
</ul>
有序列表ol

<ol>
<li>Coffee</li>
<li>Milk</li>
</ol>
HTML 自定义列表
自定义列表不仅仅是一列项目，而是项目及其注释的组合。
自定义列表以 `<dl> `标签开始。每个自定义列表项以 `<dt>` 开始。每个自定义列表项的定义以` <dd>` 开始。
```
<dl>
<dt>Coffee</dt>
<dd>- black hot drink</dd>
<dt>Milk</dt>
<dd>- white cold drink</dd>
</dl>
```
> ul是unordered lists的缩写 (无序列表)
> li是list item的缩写 （列表项目）
> ol是ordered lists的缩写（有序列表）
> dl是definition lists的英文缩写 (自定义列表)
> dt是definition term的缩写 (自定义列表组)
> dd是definition description的缩写（自定义列表描述）
> nl是navigation lists的英文缩写 （导航列表）
> tr是table row的缩写 （表格中的一行）
> th是table header cell的缩写 （表格中的表头）
> td是table data cell的缩写 （表格中的一个单元格）

### 7表单

**文本域（Text Fields）**
文本域通过<input type="text"> 标签来设定，当用户要在表单中键入字母、数字等内容时，就会用到文本域。

<form>
First name: <input type="text" name="firstname"><br>
Last name: <input type="text" name="lastname">
</form>

**密码字段**

密码字段通过标签<input type="password"> 来定义:

<form>
Password: <input type="password" name="pwd">
</form>

**单选按钮（Radio Buttons）**

<input type="radio"> 标签定义了表单单选框选项

<form>
<input type="radio" name="sex" value="male">Male<br>
<input type="radio" name="sex" value="female">Female
</form>

**复选框（Checkboxes）**

<input type="checkbox"> 定义了复选框. 用户需要从若干给定的选择中选取一个或若干选项。

<form>
<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
<input type="checkbox" name="vehicle" value="Car">I have a car
</form>

**提交按钮(Submit Button)**

<input type="submit"> 定义了提交按钮.

当用户单击确认按钮时，表单的内容会被传送到另一个文件。表单的动作属性定义了目的文件的文件名。由动作属性定义的这个文件通常会对接收到的输入数据进行相关的处理。:

<form name="input" action="html_form_action.php" method="get">
Username: <input type="text" name="user">
<input type="submit" value="Submit">
</form>

假如您在上面的文本框内键入几个字母，然后点击确认按钮，那么输入数据会传送到 "html_form_action.php" 的页面。该页面将显示出输入的结果。

### HTML5新标签

上面在标签是h5以前就有的，h5为了适应移动设备等原因加了很多新标签。

video

audio
